{
  config.autoCmd = [
    {
      event = "FileType";
      pattern = "nix";
      command = "setlocal tabstop=2 shiftwidth=2";
    }
    {
      event = "FileType";
      pattern = ["markdown" "org" "gitcommit" "txt"];
      command = "setlocal spell spelllang=en_us";
      desc = "Turn on spellcheck";
    }
    {
      event = "BufEnter";
      pattern = "*.md";
      callback = {
        __raw = ''
          function()
            vim.opt_local.conceallevel = 1

            vim.cmd [[
              syn match todoCheckbox '\[\s\]' conceal cchar=
              syn match todoCheckbox '\v\[(x|X)\]' conceal cchar=
              syn match todoCheckbox '\[-\]' conceal cchar=

              syn match todoCheckbox '```' conceal cchar=
            ]]
          end
        '';
      };
      desc = "Turn on conceal chars";
    }
    {
      event = ["BufReadPost"];
      pattern = "*";
      callback = {
        __raw = ''
          function()
            local line = vim.fn.line("'\"")
            if line >= 1 and line <= vim.fn.line("$")
              and vim.bo.filetype ~= 'commit'
              and vim.fn.index({'xxd', 'gitrebase'}, vim.bo.filetype) == -1 then
              vim.cmd('normal! g`"')
            end
          end
        '';
      };
    }
    {
      event = ["BufWritePost"];
      pattern = "*";
      command = ''if getline(1) =~ "^#!" | if getline(1) =~ "/bin/" | silent execute "!chmod +x <afile>" | endif | endif'';
      desc = "Set scripts to be executable from the shell";
    }
    {
      event = "TextYankPost";
      pattern = "*";
      callback = {
        __raw = ''
          function()
            require("vim.highlight").on_yank{ higroup = "Search", timeout = 100}
          end
        '';
      };
    }
    {
      event = "LspAttach";
      pattern = "*";
      callback = {
        __raw = ''
          function(args)
          vim.bo[args.buf].tagfunc = ""
          end
        '';
      };
    }
  ];
}
