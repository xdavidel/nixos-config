{
  inputs,
  config,
  pkgs,
  user,
  ...
}: {
  wsl.enable = true;
  wsl.defaultUser = "${user}";
  wsl.nativeSystemd = true;

  users.users.${user}.isNormalUser = true;

  environment.shells = with pkgs; [
    zsh
  ];
  users.defaultUserShell = pkgs.zsh;

  programs.zsh.enable = true;

  programs.git.enable = true;

  system.stateVersion = "23.05";
}
