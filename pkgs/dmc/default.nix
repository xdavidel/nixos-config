{pkgs}:
pkgs.writeShellApplication {
  name = "dmc";
  runtimeInputs = with pkgs; [
    coreutils
    getopt
    mpc-cli
    pavucontrol
    wireplumber
  ];
  text = builtins.readFile ./dmc;
}
