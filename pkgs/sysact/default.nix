{pkgs}:
pkgs.writeShellApplication {
  name = "sysact";
  runtimeInputs = with pkgs; [
    bemenu
    coreutils
    getopt
    procps
    util-linux
  ];
  text = builtins.readFile ./sysact;
}
