(use-package evil
  :init
  (setq evil-want-Y-yank-to-eol t
        evil-want-integration t
        evil-want-keybinding nil
        evil-want-C-u-scroll t
        evil-want-C-i-jump nil
        evil-respect-visual-line-mode t
        evil-undo-system 'undo-redo
        evil-vsplit-window-right t
        evil-split-window-below t)
  :config
  (evil-mode 1)
  (evil-set-leader 'normal " ")
  ;; Rebind `universal-argument' to 'C-M-u' since 'C-u' now scrolls the buffer
  (global-set-key (kbd "C-M-u") 'universal-argument)

  ;; Use visual line motions even outside of visual-line-mode buffers
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)

  ;; Remove those evil bindings
  (with-eval-after-load 'evil-maps
    (define-key evil-normal-state-map (kbd "gr") 'xref-find-references)
    (define-key evil-normal-state-map (kbd "C-n") nil)
    (define-key evil-normal-state-map (kbd "C-p") nil)
    (define-key evil-motion-state-map (kbd "SPC") nil)
    (define-key evil-motion-state-map (kbd "RET") nil)
    (define-key evil-motion-state-map (kbd "TAB") nil))

  ;; make evil-search-word look for symbol rather than word boundaries
  (with-eval-after-load 'evil
    (defalias #'forward-evil-word #'forward-evil-symbol)
    (setq-default evil-symbol-word-search t)))

;; Comment code efficiently
(use-package evil-nerd-commenter
             :bind (:map global-map
                         ("C-/" . evilnc-comment-or-uncomment-lines)
                    :map evil-normal-state-map
                         ("gcc" . evilnc-comment-or-uncomment-lines)
                    :map evil-visual-state-map
                         ("gc" . evilnc-comment-or-uncomment-lines))
             :after evil
             :commands (evilnc-comment-or-uncomment-lines))
