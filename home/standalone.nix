{
  inputs,
  lib,
  pkgs,
  config,
  outputs,
  ...
}: let
  user = config.home.username;
in {
  imports = [./global];

  home = {
    username = lib.mkDefault "user";
    homeDirectory = "/home/${user}";
    sessionPath = ["$HOME/.local/bin"];
    sessionVariables = {
      PATH = "$PATH:/home/${user}/.nix-profile/bin";
    };
  };

  xdg = {
    # Add Nix Packages to XDG_DATA_DIRS
    enable = true;
    systemDirs.data = ["/home/${user}/.nix-profile/share"];
  };

  nixpkgs.config.allowUnfree = true; # Allow Proprietary Software.
}
