{pkgs, ...}: {
  colorschemes.catppuccin = {
    enable = true;
    integrations.cmp = true;
    transparentBackground = true;
  };
}
