{pkgs, ...}: {
  extraPlugins = let
    arachne = pkgs.vimUtils.buildVimPlugin {
      name = "arachne";
      src = pkgs.fetchFromGitHub {
        owner = "xdavidel";
        repo = "arachne.nvim";
        rev = "69df75c24091c8fe9ca5287a12ade37916667198";
        sha256 = "JjQBJrhg1QZqpKob5KaJu0qdEvpldYGQvN2nWrV+H+4=";
      };
    };
  in
    with pkgs.vimPlugins; [
      arachne
    ];
  extraConfigLua = ''
    local note_dir = "~/Documents/notes"
    require('arachne').setup {
      notes_directory = vim.fn.expand(note_dir),
      file_extension = '.md',
    }

    local function find_notes()
      local ok, telescope_builtin = pcall(require, 'telescope.builtin')
      if not ok then return end

      telescope_builtin.find_files {
        prompt_title = '<notes::files>',
        cwd = vim.fn.expand(note_dir),
      }
    end

    local function grep_notes()
      local ok, telescope_builtin = pcall(require, 'telescope.builtin')
      if not ok then return end

      telescope_builtin.live_grep {
        prompt_title = '<notes::grep>',
        cwd = vim.fn.expand(note_dir),
      }
    end

    vim.keymap.set('n', '<leader>nn', require('arachne').new, { desc = '[N]ote [N]ew' })

    vim.keymap.set('n', '<leader>nN', function()
      vim.ui.input({prompt = "File extension: "}, function(input)
        require('arachne').new(input)
      end) end , { desc = '[N]ote [N]ew typed' })

    vim.keymap.set('n', '<leader>nr', require('arachne').rename, { desc = '[N]ote [R]ename' })

    vim.keymap.set('n', '<leader>nf', find_notes, { desc = '[N]ote [F]ind' })
    vim.keymap.set('n', '<leader>ng', grep_notes, { desc = '[N]ote [G]rep' })
  '';
}
