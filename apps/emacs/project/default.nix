{pkgs, ...}: {
  programs.emacs = {
    extraPackages = epkgs:
      with epkgs; [
        projectile
      ];
    extraConfig = builtins.readFile ./project.el;
  };
}
