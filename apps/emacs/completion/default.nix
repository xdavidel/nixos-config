{pkgs, ...}: {
  programs.emacs = {
    extraPackages = epkgs:
      with epkgs; [
        vertico
        marginalia
        orderless
        company
        company-prescient
        company-emoji
        consult
      ];
    extraConfig = ''
      ${builtins.readFile ./completion.el}


      (setq consult-find-args "${pkgs.findutils}/bin/find -type f")
      (setq consult-find-args "${pkgs.fd}/bin/fd -tf -tl -tx --full-path --color=never")
    '';
  };
}
