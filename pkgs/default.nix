{pkgs ? import <nixpkgs> {}}: rec {
  # Personal scripts
  autostart = pkgs.callPackage ./autostart {};
  chs = pkgs.callPackage ./chs {};
  binrev = pkgs.callPackage ./binrev {};
  bulkmv = pkgs.callPackage ./bulkmv {};
  dmc = pkgs.callPackage ./dmc {};
  flakeup = pkgs.callPackage ./flakeup {};
  flatdir = pkgs.callPackage ./flatdir {};
  getwal = pkgs.callPackage ./getwal {};
  hashes = pkgs.callPackage ./hashes {};
  hyprmon = pkgs.callPackage ./hyprmon {};
  iconsel = pkgs.callPackage ./iconsel {};
  likecritty = pkgs.callPackage ./likecritty {};
  listfiles = pkgs.callPackage ./listfiles {};
  screenshot = pkgs.callPackage ./screenshot {};
  setbg = pkgs.callPackage ./setbg {};
  sysact = pkgs.callPackage ./sysact {};
  togglefloatfocus = pkgs.callPackage ./togglefloatfocus {};
  vcompress = pkgs.callPackage ./vcompress {};
}
