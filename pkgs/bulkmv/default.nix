{pkgs}:
pkgs.writeShellApplication {
  name = "bulkmv";
  runtimeInputs = with pkgs; [
    gnused
    coreutils
  ];
  text = builtins.readFile ./bulkmv;
}
