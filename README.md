# Nix Configuration

This is a simple attempt at using NixOS

## NIXOS Fresh install

### Partitioning

First we need to create the partitions for
our system.

This include at least a `boot` partition and
a `root` partition.

After that we need to create a file system
for each partition:

```sh
mkfs.ext4 -L nixos /dev/sda2
mkfs.fat -F32 -n boot /dev/sda1
```

Now we mount the partitions:

```sh
mount /dev/sda2 /mnt
mkdir -p /mnt/boot/efi
mount /dev/sda1 /mnt/boot/efi
```

Finally we create the needed `hardware-configuration.nix` file:

```sh
nixos-generate-config
```

And we copy this file here.

### Setup Host

We use this command to apply host from the flake:

```sh
nixos-install --flake .#<host>
```

### Rebuilding

```sh
nixos-rebuild --flake .#<host> switch
```

## WSL Configuration

In the flake file: `nixosConfigurations.wsl`

This configuration suits for nixos for wsl
[NixOS-WSL](https://github.com/nix-community/NixOS-WSL).

### Installing

1. Clone this repository
2. Change directory to it
3. Run

```sh
nixos-rebuild switch --flake .#wsl
```

## RaspberryPI Configuration

In the flake file: `nixosConfigurations.rpi`

This configuration suits for building image for
raspberry pi 3. The build process will probably take
place on a different stronger machine so the deployment
is a little different. (We don't want to build stuff on
a raspberry...)

More info can be read at the [nix wiki](https://nixos.wiki/wiki/NixOS_on_ARM)

### Building

1. Clone this repository
2. Change directory to it
3. Binfmt using Qemu
Make sure to have binfmt install correctly in the
system. Also, the nix daemon must know about the ability
to compile for other architectures.

Add the following line to `/etc/nix/nix.conf`:

```conf
extra-platforms = aarch64-linux arm-linux
```

Restart the nix daemon

```sh
systemctl restart nix-daemon.service
```

4. Run

```sh
nix build .#nixosConfigurations.rpi.config.system.build.sdImage
```

### Deploying

After a succesfull build we can expect our image to be present in a path
similar to:
`result/sd-image/nixos-sd-image-23.11.20231117.a71323f-aarch64-linux.img.zst`.

To extract it we can use the command:

```sh
unzstd -o nixospi.img result/sd-image/nixos-sd-image-23.11.20231117.a71323f-aarch64-linux.img.zst
```

After that it is possible to flash the image into the sd card:

```sh
dd if=nixospi.img of=/dev/sdb status=progress bs=4M
```

## Nix on Droid

In the flake file: `nixOnDroidConfigurations`

### Installation

To apply the configuration:

```sh
nix-on-droid switch --flake .
```

## Home Manager 

In the flake file: `homeConfigurations`

This is configuration for standalone home-manager.
Can be used for distros other than `NixOS` or custom versions
of it like `NixOS-WSL` and `nix-on-droid`.

### Installation

1. Install Nix
First install nix using the distro package manager (Arch, Debian, etc.),
or by using the installation script at nixos website.

Depending on the distro - the `/nix` directory might need to
be created manually:

```sh
install -d -m755 -o $(id -u) -g $(id -g) /nix
```

2. Enable extra features

Nix command and flakes are in use here.
To set them up in the configuration:

```sh
mkdir -p ~/.config/nix
echo "experimental-features = nix-command flakes" >> ~/.config/nix/nix.conf
```

3. Apply the configuration

For the first deployment of the configuration - `home-manager` and other
tools are not yet available.

- Using nix build
  First build the configuration while specifying the system:

  ```sh
  nix build .#homeConfigurations.x86_64-linux.user.activationPackage
  ```

  This will create a `result` directory. To apply it:

  ```sh
  ./result/activate
  ```

- Using nix run
  The following command will run `home-manger` from Github
  to apply the configuration

  ```sh
  nix run home-manager/master -- switch --flake .#user
  ```
