{pkgs}:
pkgs.writeShellApplication {
  name = "getwal";
  runtimeInputs = with pkgs; [
    coreutils
    curl
  ];
  text = builtins.readFile ./getwal;
}
