{pkgs}:
pkgs.writeShellApplication {
  name = "hyprmon";
  runtimeInputs = with pkgs; [
    coreutils
    gnused
    jq
    socat
    wdisplays
  ];
  text = builtins.readFile ./hyprmon;
}
