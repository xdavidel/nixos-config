{
  pkgs,
  graphical,
  ...
}: {
  extraPlugins = [
    pkgs.vimPlugins.plantuml-syntax
  ];
  extraConfigLua =
    if (graphical == true)
    then ''
      local group = vim.api.nvim_create_augroup('Plantuml', { clear = true })
      vim.api.nvim_create_autocmd('BufReadPost', {
        pattern = { '*.plantuml' },
        callback = function(event)
          vim.keymap.set('n', '<leader>cc', function()
            vim.cmd [[silent !${pkgs.plantuml}/bin/plantuml -tsvg % ]]
            vim.print("Document Compiled")
          end, { buffer = event.buf, desc = '[C]ode [C]ompile' })
        end,
        group = group,
      })
    ''
    else "";
}
