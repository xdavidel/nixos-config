{pkgs}:
pkgs.writeShellApplication {
  name = "chs";
  runtimeInputs = with pkgs; [
    curl
    fzf
    coreutils
  ];
  text = builtins.readFile ./chs;
}
