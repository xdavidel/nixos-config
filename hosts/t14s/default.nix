{
  pkgs,
  inputs,
  ...
}: {
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix
    ../../nixos/general.nix
    ../../nixos/desktop/bluetooth.nix
    ../../nixos/desktop/common.nix
    ../../nixos/desktop/wayland.nix
    inputs.home-manager.nixosModules.home-manager
  ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.systemd-boot.configurationLimit = 5;
  boot.loader.efi.canTouchEfiVariables = true;

  zramSwap.enable = true;

  services.fstrim.enable = true;

  services.blueman.enable = true;

  boot.kernelPackages = pkgs.linuxPackages_zen;

  networking.hostName = "tp-dd"; # Define your hostname.

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Asia/Jerusalem";

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.user = {
    isNormalUser = true;
    description = "User";
    extraGroups = ["networkmanager" "wheel" "input"];
    packages = with pkgs; [
      firefox
      home-manager
    ];
  };

  users.defaultUserShell = pkgs.zsh;
  programs.zsh.enable = true;

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  system.stateVersion = "23.11";
}
