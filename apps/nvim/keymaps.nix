{
  config = {
    plugins.which-key = {
      enable = true;
      showKeys = true;
      registrations = {
        "<leader>b" = "Nvim Tree";
        "<leader>t" = "Toggle";
        "<leader>g" = "Git";
        "<leader>l" = "Lsp";
        "<leader>s" = "Search";
        "g" = "GoTo";
        "<leader>c" = "Code";
        "<leader>w" = "Workspace";
        "<leader>f" = "Find";
        "<leader>fc" = "Calls";
        "<leader>fs" = "Symbols";
      };
    };
    keymaps = [
      {
        mode = ["n"];
        key = "<Space>";
        action = "<Nop>";
        options.desc = "Leader key";
      }
      {
        mode = ["n"];
        key = "<M-z>";
        action = ":set wrap!<cr>";
        options.desc = "Toggle wrap mode";
      }
      {
        mode = ["n"];
        key = "<leader>tc";
        action = ":setlocal <C-R>=&conceallevel ? 'conceallevel=0' : 'conceallevel=1'<CR><CR>";
        options.desc = "[T]oggle [C]onceallevel";
      }
      {
        mode = ["i"];
        key = "<M-z>";
        action = "<Esc>:set wrap!<cr>a";
        options.desc = "Toggle wrap mode";
      }
      {
        mode = ["n"];
        key = "<C-A-Up>";
        action = ":resize -2<CR>";
        options.desc = "Resize window up";
      }
      {
        mode = ["n"];
        key = "<C-A-Down>";
        action = ":resize +2<CR>";
        options.desc = "Resize window down";
      }
      {
        mode = ["n"];
        key = "<C-A-Left>";
        action = ":vertical resize -2<CR>";
        options.desc = "Resize window left";
      }
      {
        mode = ["n"];
        key = "<C-A-Right>";
        action = ":vertical resize +2<CR>";
        options.desc = "Resize window right";
      }
      {
        mode = ["t"];
        key = "<Esc>";
        action = ''<C-\><C-n>'';
        options.desc = "Escape insert mode in terminal";
      }
      {
        mode = ["t"];
        key = "<Esc><Esc>";
        action = "<Esc>";
        options.desc = "Nornal escape in terminal";
      }
      {
        mode = ["v"];
        key = "<";
        action = "<gv";
        options.desc = "Indent left";
      }
      {
        mode = ["v"];
        key = ">";
        action = ">gv";
        options.desc = "Indent right";
      }
      {
        mode = ["x"];
        key = "*";
        action = "y/\\V<C-R>\"<CR>";
        options.desc = "Search forward";
      }
      {
        mode = ["x"];
        key = "#";
        action = "y?\\V<C-R>\"<CR>";
        options.desc = "Search backward";
      }
      {
        mode = ["n"];
        key = "tn";
        action = ":tabnext<CR>";
        options.desc = "Next tab";
      }
      {
        mode = ["n"];
        key = "tp";
        action = ":tabprevious<CR>";
        options.desc = "Prev tab";
      }
      {
        mode = ["n"];
        key = "tx";
        action = ":tabclose<CR>";
        options.desc = "Close tab";
      }
      {
        mode = ["n"];
        key = "tc";
        action = ":tabnew<CR>";
        options.desc = "New tab";
      }
      {
        mode = "n";
        key = "Y";
        action = "y$";
        options.desc = "Yank to the end of the line";
      }
      {
        mode = ["n"];
        key = "<A-Down>";
        action = ":m .+1<CR>==";
        options.desc = "Move line down";
      }
      {
        mode = ["n"];
        key = "<A-Up>";
        action = ":m .-2<CR>==";
        options.desc = "Move line up";
      }
      {
        mode = ["i"];
        key = "<A-Down>";
        action = "<Esc>:m .+1<CR>==gi";
        options.desc = "Move line down";
      }
      {
        mode = ["i"];
        key = "<A-Up>";
        action = "<Esc>:m .-2<CR>==gi";
        options.desc = "Move line up";
      }
      {
        mode = ["x"];
        key = "<A-Down>";
        action = ":m '>+1<CR>gv-gv";
        options.desc = "Move line down";
      }
      {
        mode = ["x"];
        key = "<A-Up>";
        action = ":m '<-2<CR>gv-gv";
        options.desc = "Move line up";
      }
      {
        mode = ["n"];
        key = "]q";
        action = ":cnext<CR>";
        options.desc = "Next quickfix";
      }
      {
        mode = ["n"];
        key = "[q";
        action = ":cprev<CR>";
        options.desc = "Prev quickfix";
      }
      {
        mode = ["n"];
        key = "n";
        action = "nzzzv";
        options.desc = "Next search (center cursor)";
      }
      {
        mode = ["n"];
        key = "N";
        action = "Nzzzv";
        options.desc = "Prev search (center cursor)";
      }
      {
        mode = ["n"];
        key = "dc";
        action = "dt_";
        options.desc = "Delete until _";
      }
      {
        mode = ["n"];
        key = "cd";
        action = "ct_";
        options.desc = "Change until _";
      }
      {
        mode = ["n"];
        key = "/";
        action = "ms/";
        options.desc = "Search forward (save position)";
      }
      {
        mode = ["n"];
        key = "?";
        action = "ms?";
        options.desc = "Search backward (save position)";
      }
      {
        mode = ["n"];
        key = "dm";
        action = ":%s/<c-r>///g<CR>";
        options.desc = "Delete matches";
      }
      {
        mode = ["n"];
        key = "cm";
        action = ":%s/<c-r>///g<Left><Left>";
        options.desc = "Change matches";
      }
      {
        key = "<leader>b";
        action = "<nop>";
        options = {
          desc = "NvimTree";
        };
      }
      {
        key = "<leader>bt";
        action = ":NvimTreeToggle<CR>";
        options = {
          silent = true;
          desc = "NvimTree Toggle";
        };
      }
      {
        key = "<leader>br";
        action = ":NvimTreeRefresh<CR>";
        options = {
          silent = true;
          desc = "Refresh";
        };
      }
      {
        key = "<leader>bf";
        action = ":NvimTreeFindFile<CR>";
        options = {
          silent = true;
          desc = "Find File";
        };
      }
      {
        key = "<leader>e";
        action = ":NvimTreeToggle<CR>";
        options = {
          silent = true;
          desc = "NvimTree Toggle";
        };
      }
    ];
  };
}
