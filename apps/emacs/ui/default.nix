{pkgs, ...}: {
  programs.emacs = {
    extraPackages = epkgs:
      with epkgs; [
        eldoc-box
        rainbow-delimiters
        rainbow-mode
      ];
    extraConfig = builtins.readFile ./ui.el;
  };
}
