{
  config,
  pkgs,
  inputs,
  lib,
  ...
}: let
  pypkgs = ps:
    with ps; [
      pandas
      requests
    ];
in {
  environment.systemPackages = with pkgs; [
    atool
    fd
    jq
    libqalculate
    lzop
    man-pages
    man-pages-posix
    magic-wormhole-rs
    mediainfo
    p7zip
    ripgrep
    sc-im
    unrar
    unzip
    (python3.withPackages pypkgs)
  ];

  # automatically building the immutable cache
  documentation.man.generateCaches = true;

  # development utilities man-pages
  documentation.dev.enable = true;

  # Optimization settings and garbage collection automation
  nix = {
    package = lib.mkDefault pkgs.nix;
    settings = {
      auto-optimise-store = true;
      experimental-features = ["nix-command" "flakes" "repl-flake"];
    };
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 7d";
    };
  };
}
