{pkgs}:
pkgs.writeShellApplication {
  name = "setbg";
  runtimeInputs = with pkgs; [
    coreutils
    file
    findutils
    libnotify
    procps
    swww
    util-linux
  ];
  text = builtins.readFile ./setbg;
}
