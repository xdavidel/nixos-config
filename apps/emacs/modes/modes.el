(use-package prog-mode
  :hook
  (prog-mode . (lambda() (when (display-graphic-p) (hl-line-mode))))
  (emacs-lisp-mode . (lambda () (add-to-list 'write-file-functions (lambda()
                                                                     (when (equal (file-name-extension buffer-file-name) "el")
                                                                       (check-parens)))))))

;; Plantuml mode
(use-package plantuml-mode
  :mode (("\\.plantuml\\'" . plantuml-mode)
         ("\\.pu\\'" . plantuml-mode)))


(use-package nix-ts-mode
  :mode "\\.nix\\'")

;; Rust - Cargo integration
(use-package cargo
  :if (executable-find "cargo")
  :hook ((rust-mode toml-mode) . cargo-minor-mode))

(use-package go-mode
  :mode ("\\.go\\'" . go-mode))

;; Syntax highlighting of TOML files
(use-package toml-mode
  :mode ("\\.toml\\'" . toml-mode))

;; Syntax highlighting for zig
(use-package zig-mode
  :mode ("\\.zig\\'" . zig-mode))

;; Lisp and ELisp mode
(use-package elisp-mode
  :hook (emacs-lisp-mode . eldoc-mode))

(use-package dockerfile-mode
  :mode "\\Dockerfile\\'")

(use-package markdown-mode
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'"       . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :config
  (dolist (face '((markdown-header-face-1 . 1.4)
                  (markdown-header-face-2 . 1.3)
                  (markdown-header-face-3 . 1.2)
                  (markdown-header-face-4 . 1.1)
                  (markdown-header-face-5 . 1.0)))
    (set-face-attribute (car face) nil :weight 'normal :height (cdr face)))
  :custom
  (markdown-fontify-code-blocks-natively t)
  (markdown-command "pandoc")
  (markdown-hr-display-char nil)
  (markdown-list-item-bullets '("-")))

(use-package hexl
  :init
  ;; Auto open binary files with `hexl-mode' if no other mode deteced.
  (add-to-list 'magic-fallback-mode-alist '(buffer-binary-p . hexl-mode) t)
  :config
  (setq hexl-bits 8))


;; Hacky way for Treesitter to override modes
(setq major-mode-remap-alist
 '((yaml-mode . yaml-ts-mode)
   (bash-mode . bash-ts-mode)
   (js2-mode . js-ts-mode)
   (typescript-mode . typescript-ts-mode)
   (json-mode . json-ts-mode)
   (css-mode . css-ts-mode)
   (c-mode . c-ts-mode)
   (c++-mode . c++-ts-mode)
   (python-mode . python-ts-mode)))
