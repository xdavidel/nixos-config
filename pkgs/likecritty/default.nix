{pkgs}: let
  pidof = "${pkgs.procps}/bin/pidof";
  alacritty = "${pkgs.alacritty}/bin/alacritty";
  setsid = "${pkgs.util-linux}/bin/setsid";
in
  pkgs.writeScriptBin "likecritty" ''
    #!/bin/sh

    ${pidof} -s ${alacritty} && \
         ${alacritty} msg create-window --working-directory=$HOME $@ || \
         ${setsid} -f ${alacritty} $@
  ''
