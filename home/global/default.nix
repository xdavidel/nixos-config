{
  config,
  inputs,
  lib,
  outputs,
  pkgs,
  ...
}: let
  myAliases = {
    adbshell = "${pkgs.android-tools}/bin/adb shell -t 'export HOME=/data/local/tmp'";
    cat = "${pkgs.bat}/bin/bat -pp";
    diff = "${pkgs.diffutils}/bin/diff --color=auto";
    ftrace = "${pkgs.strace}/bin/strace -e trace=open,openat,connect,accept";
    grep = "${pkgs.gnugrep}/bin/grep --color=auto";
    ip = "${pkgs.iproute2}/bin/ip -color=auto";
    l = "${pkgs.eza}/bin/eza -l --group-directories-first";
    ll = "${pkgs.eza}/bin/eza -la --group-directories-first";
    la = "${pkgs.eza}/bin/eza -a --group-directories-first";
    ls = "${pkgs.eza}/bin/eza --group-directories-first";
    lg = "${pkgs.lazygit}/bin/lazygit";
    man = "batman";
    q = "exit";
    yt = "${pkgs.yt-dlp}/bin/yt-dlp --embed-metadata -i";
    ytv = "${pkgs.yt-dlp}/bin/yt-dlp -o '%(upload_date)s-%(title)s.%(ext)s'";
    yta = "${pkgs.yt-dlp}/bin/yt-dlp -x -f bestaudio/best --restrict-filenames --audio-format mp3";
    ".." = "cd ..";
  };
  lfcd = ''
    lfcd () {
      cd "$(command ${pkgs.lf}/bin/lf -print-last-dir "$@")"
    }

    alias lf="lfcd"
  '';
  cdi = ''
    cdi () {
      cd "$(dirname "$(${pkgs.fzf}/bin/fzf)")"
    }
  '';
  lsf = ''
    lsf () {
      ${inputs.self.packages.${pkgs.system}.listfiles}/bin/listfiles
    }
  '';
  nixvim' = inputs.nixvim.legacyPackages.${pkgs.system};
  nvim = nixvim'.makeNixvimWithModule {
    inherit pkgs;
    extraSpecialArgs = {graphical = config.graphical.option;};
    module = import ../../apps/nvim;
  };
  nvim-desktop = pkgs.makeDesktopItem {
    name = "nvim-desktop";
    desktopName = "Neovim Desktop";
    genericName = "Text Editor";
    exec = ''${config.home.sessionVariables.TERMINAL} -e ${nvim}/bin/nvim %u'';
    icon = "nvim";
    categories = ["Utility" "TextEditor"];
    mimeTypes = [
      "text/plain"
      "text/x-shellscript"
      "text/english"
      "text/x-makefile"
      "text/x-c++hdr"
      "text/x-c++src "
      "text/x-chdr"
      "text/x-csrc"
      "text/x-java"
      "text/x-moc"
      "text/x-pascal"
      "text/x-tcl"
      "text/x-tex"
      "text/x-c"
      "text/x-c++"
    ];
  };
in {
  imports = [
    inputs.nix-colors.homeManagerModules.default
    ../../apps/emacs
  ];

  options = {
    graphical.option = lib.mkOption {
      type = lib.types.bool;
      default = false;
    };
  };

  config = {
    colorScheme = inputs.nix-colors.colorSchemes.catppuccin-mocha;

    programs.lf = {
      enable = true;

      commands = {
        dragon-out = ''%${pkgs.xdragon}/bin/xdragon -a -x "$fx"'';
        destroy = ''%rm -rf "$f"'';
        editor-open = ''$$EDITOR $f'';
        chmod = ''
          ''${{
            echo -n "Mode Bits: "
            read ans

            for file in "$fx"; do
            chmod $ans $file
            done
          }}'';
        chown = ''
          ''${{
            echo -n "Owner: "
            read ans

            for file in "$fx"; do
            chown $ans:$ans $file
            done
          }}'';
      };

      keybindings = {
        "." = "set hidden!";
        "<enter>" = "open";
        B = ''''$${pkgs.binwalk}/bin/binwalk -e "$f"'';
        c = "";
        cw = ''''$${inputs.self.packages.${pkgs.system}.bulkmv}/bin/bulkmv "$fs"'';
        cm = "chmod";
        co = "chown";
        d = "";
        dd = "cut";
        dD = "delete";
        do = "dragon-out";
        D = "destroy";
        e = "editor-open";
        E = ''''$${pkgs.atool}/bin/aunpack "$f"'';
        m = "";
        md = "push %mkdir<space>";
        mf = "push %touch<space>";
        S = ''''$${inputs.self.packages.${pkgs.system}.hashes}/bin/hashes "$fx" | $EDITOR -'';
        V = ''''$${pkgs.bat}/bin/bat --paging=always --theme=gruvbox "$f"'';
        w = ''''$${inputs.self.packages.${pkgs.system}.setbg}/bin/setbg "$f"'';
      };

      settings = {
        preview = true;
        hidden = false;
        ignorecase = true;
        previewer = "${pkgs.ctpv}/bin/ctpv";
        cleaner = "${pkgs.ctpv}/bin/ctpvclear";
      };
    };

    programs.eza = {
      enable = true;
      extraOptions = [
        "--group-directories-first"
        "--header"
      ];
    };

    programs.bat = {
      enable = true;
      extraPackages = with pkgs.bat-extras; [batman];
    };

    programs.readline = {
      enable = true;
      extraConfig = ''
        set editing-mode vi
        $if mode=vi
        set show-mode-in-prompt on

        $if term=linux
        set vi-cmd-mode-string "[N] \1\e[?0c\2"
        set vi-ins-mode-string "[I] \1\e[?8c\2"
        $else
        set vi-cmd-mode-string "\1\e[30;41m\2[N]\1\e[m\2 \1\e[2 q\2"
        set vi-ins-mode-string "\1\e[30;42m\2[I]\1\e[m\2 \1\e[6 q\2"
        $endif
        set keymap vi-command
          # these are for vi-command mode
          Control-l: clear-screen
          Control-a: beginning-of-line

          set keymap vi-insert
          # these are for vi-insert mode
          Control-l: clear-screen
          Control-a: beginning-of-line

          $endif

        # Fast vi mode switching
        set keyseq-timeout 1

        # Search history for partial commands completions
        "\e[A": history-search-backward
        "\e[B": history-search-forward

        # Single tab completion
        set show-all-if-ambiguous on
        set completion-ignore-case On
      '';
    };

    programs.bash = {
      enable = true;
      shellAliases = myAliases;
      enableCompletion = true;
      historyControl = ["erasedups" "ignoredups" "ignorespace"];
      bashrcExtra = ''
        ${lfcd}
        ${cdi}
        ${lsf}
        bind '"\C-o":"lfcd\C-m"'
        bind '"\C-f":"cdi\C-m"'
        bind '"\C-p":"lsf\C-m"'

      '';
    };

    programs.zsh = {
      enable = true;
      shellAliases = myAliases;
      autosuggestion.enable = true;
      enableCompletion = true;
      # defaultKeymap = "vicmd";
      syntaxHighlighting.enable = true;
      dotDir = ".config/zsh";
      history = {
        ignoreAllDups = true;
        ignorePatterns = ["yta *" "ytv *"];
        ignoreSpace = true;
        path = "$ZDOTDIR/.zsh_history";
        share = false;
      };

      initExtra = ''
        zstyle ':completion:*' menu select
        zstyle ':completion:*' matcher-list ''' 'm:{a-zA-Z}={A-Za-z}' 'r:|=*' 'l:|=* r:|=*'
        zstyle ':completion:*' rehash true

        zstyle ':completion:*' accept-exact '*(N)'
        zstyle ':completion:*' use-cache on
        zstyle ':completion:*' cache-path ~/.cache/zsh/cache

        ${lfcd}
        ${cdi}
        ${lsf}
        bindkey -s '^o' 'lfcd\n'
        bindkey -s '^f' 'cdi\n'
        bindkey -s '^p' 'lsf\n'
      '';
    };

    programs.pandoc = {
      enable = true;
    };

    programs.ssh = {
      enable = true;
      compression = true;
      addKeysToAgent = "yes";
      forwardAgent = true;
      hashKnownHosts = true;
      includes = ["~/.ssh/config.d/*.conf"];
    };

    programs.starship = {
      enable = true;
      enableBashIntegration = true;
      enableZshIntegration = true;
      # settings = {
      #   character = {
      #     success_symbol = "[>](bold green)";
      #     error_symbol = "[>](bold red)";
      #   };
      # };
    };

    programs.tmux = {
      enable = true;
      keyMode = "vi";
      mouse = true;
      prefix = "C-a";
    };

    programs.fzf = {
      enable = true;
      enableBashIntegration = true;
      enableZshIntegration = true;
    };

    programs.git = {
      enable = true;
      aliases = {
        "d" = "diff";
        "gl" = "config --global -l";
        "co" = "checkout";
        "cm" = "commit -m";
        "mr" = "merge";
        "st" = "status -sb";
        "ll" = "log --oneline";
        "rv" = "remote -v";
        "se" = "!git rev-list --all | ${pkgs.findutils}/bin/xargs git grep -F";
        "last" = "log -1 HEAD --stat";
      };
      package = pkgs.gitFull;
      extraConfig = {
        credential.helper = "${pkgs.gitFull}/bin/git-credential-libsecret";
      };
    };

    programs.home-manager.enable = true;
    manual.manpages.enable = true;

    home.packages = with pkgs;
      [
        chafa
        clang
        ffmpeg
        file
        gdb
        glow
        htop
        imagemagick
        inputs.self.packages.${pkgs.system}.chs
        inputs.self.packages.${pkgs.system}.flatdir
        inputs.self.packages.${pkgs.system}.getwal
        inputs.self.packages.${pkgs.system}.vcompress
        jq
        lazygit
        newsboat
        nvim
        ripgrep
        unixtools.xxd
        yt-dlp
      ]
      ++ (
        if (config.graphical.option == true)
        then [nvim-desktop]
        else []
      );

    home.sessionVariables = {
      EDITOR = "nvim";
    };

    home.stateVersion = "23.05";
  };
}
