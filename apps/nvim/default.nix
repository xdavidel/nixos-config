{pkgs, ...}: {
  # Import all your configuration modules here
  imports = [
    ./theme.nix
    ./autocmd.nix
    ./keymaps.nix
    ./lsp.nix
    ./completions.nix
    ./plugins.nix
  ];

  config = {
    enableMan = true;
    viAlias = true;
    vimAlias = true;
    globals = {
      mapleader = " ";
      maplocalleader = " ";
    };

    luaLoader.enable = true;

    options = {
      background = "dark";
      number = true;
      relativenumber = true;

      incsearch = true;
      ignorecase = true;
      smartcase = true;

      tabstop = 4;
      softtabstop = -1;
      shiftwidth = 4;
      expandtab = true;

      clipboard = "unnamedplus";

      splitbelow = true;
      splitright = true;

      # No Wrapping
      wrap = false;

      writebackup = false;
      swapfile = false;
      backup = false;

      undofile = true;

      signcolumn = "yes";

      foldmethod = "expr";
      foldlevelstart = 99;
    };

    extraConfigLuaPre = ''
      local default_diagnostic_config = {
        signs = {
          active = true,
          values = {
            { name = "DiagnosticSignError", text = "" },
            { name = "DiagnosticSignWarn", text = "" },
            { name = "DiagnosticSignHint", text = "" },
            { name = "DiagnosticSignInfo", text = "" },
          },
        },
        virtual_text = false,
        update_in_insert = false,
        underline = true,
        severity_sort = true,
        float = {
          focusable = true,
          style = "minimal",
          border = "rounded",
          source = "always",
          header = "",
          prefix = "",
        },
      }

      vim.diagnostic.config(default_diagnostic_config)
      vim.keymap.set({'n'}, 'gl',  '<cmd>lua vim.diagnostic.open_float()<CR>', {desc="diagnostic"})
    '';

    highlight.ExtraWhitespace.bg = "red";
    match.ExtraWhitespace = "\\s\\+$";

    extraPackages = with pkgs; [xclip wl-clipboard];
  };
}
