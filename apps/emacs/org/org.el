(use-package org
  :init
  ;; Return or left-click with mouse follows link
  (setq org-return-follows-link t
        org-mouse-1-follows-link t
        org-descriptive-links t) ;; Display links as the description provided
  :mode (("\\.org$" . org-mode))
  :custom
  (org-hide-emphasis-markers t)
  (org-confirm-babel-evaluate nil)     ; Don't ask before code evaluation
  (org-startup-with-inline-images nil) ; Don't show inline images at startup
  (org-edit-src-content-indentation 0) ; Indentation size for org source blocks
  (org-hide-leading-stars t)           ; One start is fine
  (org-tags-column 0)                  ; tag right after text
  (org-ellipsis " ▼")
  (org-log-done 'time)
  (org-journal-date-format "%B %d, %Y (%A) ")
  (org-journal-file-format "%Y-%m-%d.org")
  (org-directory (let ((dir (file-name-as-directory (expand-file-name "~/Documents/Org"))))
                   (make-directory dir :parents)
                   dir))
  (org-default-notes-file (expand-file-name "notes.org" org-directory))
  (org-todo-keywords
   '((sequence
      "TODO(t)"
      "DOING(p)"
      "BUG(b)"
      "WAIT(w)"
      "|"                ; The pipe necessary to separate "active" states and "inactive" states
      "DONE(d)"
      "CANCELLED(c)" )))
  :config
  ;; change header size on different levels
  (dolist (face '((org-level-1 . 1.2)
                  (org-level-2 . 1.1)
                  (org-level-3 . 1.05)
                  (org-level-4 . 1.0)
                  (org-level-5 . 1.1)
                  (org-level-6 . 1.1)
                  (org-level-7 . 1.1)
                  (org-level-8 . 1.1)))
    (set-face-attribute (car face) nil :height (cdr face)))

  ;; disable auto-pairing of "<" in org-mode
  (add-hook 'org-mode-hook (lambda ()
                             (setq-local electric-pair-inhibit-predicate
                                         `(lambda (c)
                                            (if (char-equal c ?<) t (,electric-pair-inhibit-predicate c))))))

  ;; Org programming languages templates
  (when
      (not
       (version<= org-version "9.1.9"))
    (require 'org-tempo)
    (add-to-list 'org-structure-template-alist
                 '("sh" . "src shell"))
    (add-to-list 'org-structure-template-alist
                 '("el" . "src emacs-lisp"))
    (add-to-list 'org-structure-template-alist
                 '("py" . "src python"))
    (add-to-list 'org-structure-template-alist
                 '("cpp" . "src cpp"))
    (add-to-list 'org-structure-template-alist
                 '("lua" . "src lua"))
    (add-to-list 'org-structure-template-alist
                 '("go" . "src go"))))

;; org export
(use-package ox
  :after org
  :init
  (setq org-export-in-background t))

(use-package ox-html
  :after ox
  :config
  ;; don't scale svg images
  (setq org-html-head "<style> .org-svg {width: auto} </style>"))

;; for exporting to markdown
(use-package ox-md
  :after org)

(use-package ox-latex
  :after ox
  :init
  ;; change scale of latex preview in org-mode
  (setq org-format-latex-options (plist-put org-format-latex-options :scale 1.1)
        ;; org-startup-with-latex-preview t
        org-latex-image-default-width nil ; don't scale my images!
        org-latex-images-centered nil)    ; sometimes I want side-by-side images

  ;; minted code pdf export org
  (setq org-latex-listings 'minted
        org-latex-pdf-process
        '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
          "bibtex %b"
          "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
          "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))

  ;; extra latex packages for every header
  (setq org-latex-packages-alist '(("newfloat" "minted" nil)
                                   ("a4paper, margin=20mm" "geometry" nil)))

  (add-to-list 'org-latex-default-packages-alist '("colorlinks=true, linkcolor=blue, citecolor=blue, filecolor=magenta, urlcolor=cyan" "hyperref" nil)))

(use-package org-clock
  :after org
  :config
  ;; Save the running clock and all clock history when exiting Emacs, load it on startup
  (setq org-clock-persistence-insinuate t
        org-clock-persist t
        org-clock-in-resume t
        org-clock-out-remove-zero-time-clocks t
        org-clock-mode-line-total 'current
        org-duration-format (quote h:mm)))

(use-package org-src
  :after org
  :init
  ;; babel and source blocks
  (setq org-src-fontify-natively t
        org-src-window-setup 'current-window ; don't move my windows around!
        org-src-preserve-indentation t       ; preserve indentation in code
        org-adapt-indentation nil            ; no extra space... better use indent mode (virtual)
        org-edit-src-content-indentation 0   ; dont indent source code
        org-src-tab-acts-natively t          ; if t, it is slow!
        org-confirm-babel-evaluate nil))     ; doesn't ask for confirmation

;; Avoid `org-babel-do-load-languages' since it does an eager require.

;; load ob-python only when executing python block
(use-package ob-python
  :after org
  :commands org-babel-execute:python
  :init
  (setq org-babel-default-header-args:python
        '((:results . "output")
          (:noweb . "no-export") ; referencing other blocks with <<>> syntax, don't expand during export
          (:eval . "never-export") ; don't eval blocks when exporting, except when `:eval yes`
          (:exports . "results")))) ; export only plots by default

(use-package ob-shell
  :commands
  (org-babel-execute:sh
   org-babel-expand-body:sh

   org-babel-execute:bash
   org-babel-expand-body:bash))

;; load ob-C when executing C++ source block
(use-package ob-C
  :commands org-babel-execute:C++
  :config
  (setq org-babel-default-header-args:C++
        '((:results . "output")
          (:noweb . "no-export") ; referencing other blocks with <<>> syntax, don't expand during export
          (:eval . "never-export") ; don't eval blocks when exporting, except when `:eval yes`
          (:exports . "results"))))


(use-package ob-js
  :commands org-babel-execute:js
  :config
  (setq org-babel-default-header-args:js
        '((:results . "output")
          (:noweb . "no-export") ; referencing other blocks with <<>> syntax, don't expand during export
          (:eval . "never-export") ; don't eval blocks when exporting, except when `:eval yes`
          (:exports . "results"))))

;; for UML diagrams in org-mode
;; need to install `plantuml'
(use-package ob-plantuml
  :commands org-babel-execute:plantuml
  :config
  (setq org-plantuml-exec-mode 'plantuml))

(use-package ob-core
  :after org
  :init
  ;; mkdirp allows creating the :dir if it does not exist
  (add-to-list 'org-babel-default-header-args '(:mkdirp . "yes"))
  (add-to-list 'org-babel-default-header-args '(:noweb . "no-export")))

;; Show hidden chars in org mode
(use-package org-appear
  :hook (org-mode . org-appear-mode))

;; Table of contents using `:toc:` on a heading
(use-package toc-org
  :hook
  (org-mode . toc-org-mode)
  (markdown-mode . toc-org-mode))

;; Keep text indented with headlines
(use-package org-indent
  :hook (org-mode . org-indent-mode))
