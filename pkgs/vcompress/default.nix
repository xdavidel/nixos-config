{pkgs}:
pkgs.writeShellApplication {
  name = "vcompress";
  runtimeInputs = with pkgs; [
    ffmpeg
    file
    fzf
  ];
  text = builtins.readFile ./vcompress;
}
