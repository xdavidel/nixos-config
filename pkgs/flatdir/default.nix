{pkgs}:
pkgs.writeShellApplication {
  name = "flatdir";
  runtimeInputs = with pkgs; [
    findutils
    coreutils
  ];
  text = builtins.readFile ./flatdir;
}
