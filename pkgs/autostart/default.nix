{pkgs}:
pkgs.writeShellApplication {
  name = "autostart";
  runtimeInputs = with pkgs; [
    procps
    swww
    waybar
    xdg-user-dirs
  ];
  text = builtins.readFile ./autostart;
}
