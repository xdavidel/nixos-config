{pkgs, ...}: {
  programs.emacs = {
    extraPackages = epkgs:
      with epkgs; [
        nix-ts-mode
        plantuml-mode
        cargo-mode
        rust-mode
        go-mode
        toml-mode
        zig-mode
        dockerfile-mode
        markdown-mode
        treesit-grammars.with-all-grammars
      ];
    extraConfig = builtins.readFile ./modes.el;
  };
}
