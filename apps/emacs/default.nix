{
  config,
  pkgs,
  inputs,
  ...
}: {
  home.file.".config/emacs/early-init.el".source = ./early-init.el;
  home.file.".config/emacs/init.el".source = ./init.el;

  imports = [
    ./evil
    ./completion
    ./ui
    ./modes
    ./org
    ./project
    ./shell
  ];

  programs.emacs = {
    enable = true;
    extraPackages = epkgs:
      with epkgs; [
        drag-stuff
        format-all
        magit
        vlf
        which-key
        gcmh
      ];
    extraConfig = ''
    '';
  };
}
