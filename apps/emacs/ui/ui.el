;; Emacs does not need pager
(setenv "PAGER" "cat")

;; Line numbers
(use-package display-line-numbers
  :hook
  (prog-mode . display-line-numbers-mode)
  :custom
  (display-line-numbers-width 4)
  (display-line-numbers-width-start t)
  (display-line-numbers-type 'relative))

(use-package column-number
  :hook
  (prog-mode . column-number-mode))

;; add visual pulse when changing focus, like beacon but built-in
(use-package pulse
  :unless (display-graphic-p)
  :init
  (defun pulse-line (&rest _)
    (pulse-momentary-highlight-one-line (point)))
  (dolist (cmd '(recenter-top-bottom
                 other-window windmove-do-window-select
                 pager-page-down pager-page-up
                 winum-select-window-by-number
                 ;; treemacs-select-window
                 symbol-overlay-basic-jump))
    (advice-add cmd :after #'pulse-line))) ; Highlight cursor postion after movement

;; Show eldoc information in popup
(use-package eldoc-box
  :if (display-graphic-p)
  :config
  :hook (eldoc-mode . eldoc-box-hover-at-point-mode))

;; auto-insert matching bracket
(electric-pair-mode 1)

;; Show matching parens
(use-package paren
  :init
  (setq show-paren-delay 0)
  (show-paren-mode t))

(use-package mouse
  :init
  (context-menu-mode 1))

;; Set color backgrounds to color names
(use-package rainbow-mode
  :hook (prog-mode . rainbow-mode))

;; Rainbow colors for brackets
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

