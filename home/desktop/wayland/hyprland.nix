{
  lib,
  config,
  pkgs,
  inputs,
  ...
}: let
  autorun = ''[ -z "$DISPLAY" ] && [ "$XDG_VTNR" = 1 ] && exec Hyprland'';
  gaps = 5;
  dmc = "${inputs.self.packages.${pkgs.system}.dmc}/bin/dmc";
in {
  # idle daemon
  services.swayidle = let
    locker = "${pkgs.swaylock}/bin/swaylock -f -c 010101";
    mon_on = "${pkgs.wlopm}/bin/wlopm --on '*'";
    mon_off = "${pkgs.wlopm}/bin/wlopm --off '*'";
  in {
    enable = true;
    events = [
      {
        event = "before-sleep";
        command = "${pkgs.playerctl}/bin/playerctl -a pause";
      }
      {
        event = "before-sleep";
        command = "${pkgs.systemd}/bin/loginctl lock-session";
      }
      {
        event = "lock";
        command = "${locker}";
      }
      {
        event = "after-resume";
        command = "${mon_on}";
      }
    ];
    timeouts = [
      {
        timeout = 300; # 5 minutes
        command = "${mon_off}";
        resumeCommand = "${mon_on}";
      }
    ];
    systemdTarget = "xdg-desktop-portal-hyprland.service";
  };

  programs.bash.profileExtra = "${autorun}";
  programs.zsh.profileExtra = "${autorun}";

  wayland.windowManager.hyprland = let
    wpctl = "${pkgs.wireplumber}/bin/wpctl";
  in {
    enable = true;
    xwayland.enable = true;
    # package = inputs.hyprland.packages."${pkgs.system}".hyprland;
    systemd = {
      enable = true;
      # Same as default, but stop graphical-session too
      extraCommands = lib.mkBefore [
        "systemctl --user stop graphical-session.target"
        "systemctl --user start hyprland-session.target"
      ];
    };

    settings = let
      inherit (config.colorscheme) colors;
    in {
      exec-once = "${inputs.self.packages.${pkgs.system}.autostart}/bin/autostart";
      general = {
        gaps_in = gaps;
        gaps_out = gaps;
        border_size = 2;
        cursor_inactive_timeout = 10;

        "col.active_border" = "rgba(${colors.base08}ff)";
        "col.inactive_border" = "rgba(${colors.base01}cc)";

        layout = "master";
      };
      input = {
        kb_layout = "us,il";
        kb_options = "grp:alt_shift_toggle,caps:escape";
        numlock_by_default = true;
        touchpad.disable_while_typing = true;

        repeat_delay = 300;
        repeat_rate = 50;
      };
      dwindle = {
        no_gaps_when_only = true;
        force_split = 2; # split on right and bottom
      };

      master = {
        new_is_master = true;
        new_on_top = true;
        no_gaps_when_only = 2;
      };

      layerrule = [
        "blur,waybar"
        "ignorezero,waybar"
      ];

      decoration = {
        # active_opacity = 1.0;
        # inactive_opacity = 0.75;
        fullscreen_opacity = 1.0;
        rounding = 1;
        blur = {
          enabled = false;
        };
        drop_shadow = false;
      };
      animations = {
        enabled = true;
      };

      misc = {
        vfr = true;
        disable_hyprland_logo = false;
        disable_splash_rendering = true;
        mouse_move_enables_dpms = true;
        key_press_enables_dpms = true;
        hide_cursor_on_touch = true;
        new_window_takes_over_fullscreen = 2;
      };

      binds = {
        allow_workspace_cycles = true;
      };

      "$mod" = "SUPER";

      # mouse bindings
      bindm = [
        "SUPER,mouse:272,movewindow"
        "SUPER,mouse:273,resizewindow"
      ];

      # repeatalbe bindings
      binde = let
        brightnessctl = "${pkgs.brightnessctl}/bin/brightnessctl";
        resizes = rec {
          left = "-50 0";
          right = "50 0";
          up = "0 -40";
          down = "0 40";
          h = left;
          l = right;
          k = up;
          j = down;
        };
      in
        [
          ",XF86MonBrightnessDown,exec,${brightnessctl} s 5%-"
          ",XF86MonBrightnessUp,exec,${brightnessctl} s 5%+"
          ",XF86Eject,exec,eject -T"
          ",XF86AudioMicMute,exec,pactl set-source-mute @DEFAULT_SOURCE@ toggle"
          ",XF86AudioRaiseVolume,exec, ${wpctl} set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 1%+; "
          ",XF86AudioLowerVolume,exec, ${wpctl} set-volume @DEFAULT_AUDIO_SINK@ 2%-; "
          "$mod,equal,exec, ${wpctl} set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 1%+; "
          "$mod,minus,exec, ${wpctl} set-volume @DEFAULT_AUDIO_SINK@ 2%-; "

          "$mod,bracketright,exec,${dmc} -f 5"
          "$mod,bracketleft,exec,${dmc} -b 5"
          "$mod SHIFT,bracketright,exec,${dmc} -f 10"
          "$mod SHIFT,bracketleft,exec,${dmc} -b 10"
        ]
        ++
        # Resize windows
        (lib.mapAttrsToList (
            key: resize: "$mod ALT,${key},resizeactive,${resize}"
          )
          resizes);

      # lock enabled bindings
      bindl = [
        ",switch:on:Lid Switch,exec,hyprctl keyword monitor 'eDP-1, disable'"
        ",switch:off:Lid Switch,exec,hyprctl keyword monitor ',prefered,auto,1'"
        "$mod SHIFT,escape,exec,hyprctl reload && ${pkgs.libnotify}/bin/notify-send 'Hyprland' 'Configration Reloaded' &"
        "$mod,BackSpace,exec,${inputs.self.packages.${pkgs.system}.hyprmon}/bin/hyprmon -a"
        "$mod SHIFT,BackSpace,exec,${inputs.self.packages.${pkgs.system}.hyprmon}/bin/hyprmon -w"
        "$mod CTRL,BackSpace,exec,${inputs.self.packages.${pkgs.system}.hyprmon}/bin/hyprmon -m"
        ",XF86AudioPause,exec,${dmc} -S"
        ",XF86AudioPlay,exec,${dmc} -P"
        ",XF86AudioPrev,exec,${dmc} -p"
        ",XF86AudioNext,exec,${dmc} -n"
        ",XF86AudioMute,exec,${wpctl} set-mute @DEFAULT_AUDIO_SINK@ toggle"
        "$mod,m,exec,${wpctl} set-mute @DEFAULT_AUDIO_SINK@ toggle"
        "$mod,p,exec,${dmc} -P"
        "$mod SHIFT,p,exec,${pkgs.playerctl}/bin/playerctl play-pause"
        "$mod CTRL SHIFT,p,exec,${pkgs.playerctl}/bin/playerctl -a pause"

        "$mod,comma,exec,${dmc} -p"
        "$mod,period,exec,${dmc} -n"
        "$mod SHIFT,comma,exec,${pkgs.playerctl}/bin/playerctld shift"
        "$mod SHIFT,period,exec,${pkgs.playerctl}/bin/playerctld unshift"
      ];

      bind = let
        pkill = "${pkgs.procps}/bin/pkill";
        # Map keys (arrows and hjkl) to hyprland directions (l, r, u, d)
        directions = rec {
          left = "l";
          right = "r";
          up = "u";
          down = "d";
          h = left;
          l = right;
          k = up;
          j = down;
        };
        master_nav = rec {
          up = "cycleprev";
          down = "cyclenext";
          k = up;
          j = down;
        };
        master_swap = rec {
          up = "swapprev";
          down = "swapnext";
          k = up;
          j = down;
        };
        workspace_nav = rec {
          left = "e-1";
          right = "e+1";
          h = left;
          l = right;
        };
        terminal = config.home.sessionVariables.TERMINAL;
      in
        [
          # Program bindings
          "$mod,Return,exec,${terminal}"
          "$mod,slash,exec,${pkgs.bemenu}/bin/bemenu-run -w --fn 'mono 14'"
          "$mod,q,killactive"
          "$mod,x,exec,${inputs.self.packages.${pkgs.system}.sysact}/bin/sysact -l"
          "$mod,escape,exec,${inputs.self.packages.${pkgs.system}.sysact}/bin/sysact"
          "$mod,e,exec,${pkgs.xfce.thunar}/bin/thunar"
          "$mod,b,exec,${pkill} -SIGUSR1 waybar"
          "$mod SHIFT,b,exec,${pkill} -SIGUSR2 waybar"
          "$mod SHIFT,e,exit"
          "$mod,Print,exec,${inputs.self.packages.${pkgs.system}.screenshot}/bin/screenshot"
          ",Print,exec,${inputs.self.packages.${pkgs.system}.screenshot}/bin/screenshot -f"
          "CTRL,Print,exec,${inputs.self.packages.${pkgs.system}.screenshot}/bin/screenshot -a"
          "ALT,Print,exec,${inputs.self.packages.${pkgs.system}.screenshot}/bin/screenshot -w"

          "$mod,semicolon,exec,${inputs.self.packages.${pkgs.system}.iconsel}/bin/iconsel -e"
          "$mod SHIFT,semicolon,exec,${inputs.self.packages.${pkgs.system}.iconsel}/bin/iconsel -a"

          "$mod,f,fullscreen,1"
          "$mod,y,pin,active"
          "$mod SHIFT,f,togglefloating"
          "$mod CONTROL,f,exec,${inputs.self.packages.${pkgs.system}.togglefloatfocus}/bin/togglefloatfocus"
          "$mod,Tab,workspace, previous"
          "ALT,Tab,focuscurrentorlast"
          "$mod CONTROL,up,cyclenext,prev"
          "$mod CONTROL,down,cyclenext"
          "$mod CONTROL,k,cyclenext,prev"
          "$mod CONTROL,j,cyclenext"
          "$mod SHIFT,delete,exec,hyprctl kill"
          "$mod ALT,minus,movetoworkspace, special:SP"
          "$mod ALT,return,togglespecialworkspace, SP"

          "$mod SHIFT,m,exec, ${terminal} -e ${pkgs.ncmpcpp}/bin/ncmpcpp"

          "$mod,space,layoutmsg,swapwithmaster"
          "$mod,i,layoutmsg,addmaster"
          "$mod SHIFT,i,layoutmsg,removemaster"
          "$mod,t,layoutmsg,orientationleft"
          "$mod SHIFT,t,layoutmsg,orientationtop"
          "$mod,g,layoutmsg,orientationcenter"
        ]
        ++ (lib.mapAttrsToList (
            key: direction: "$mod,${key},layoutmsg,${direction}"
          )
          master_nav)
        ++
        # Swap windows
        (lib.mapAttrsToList (
            key: direction: "$mod SHIFT,${key},layoutmsg,${direction}"
          )
          master_swap)
        ++ (lib.mapAttrsToList (
            key: direction: "$mod CONTROL,${key},workspace,${direction}"
          )
          workspace_nav)
        ++
        # Move monitor focus
        (lib.mapAttrsToList (
            key: direction: "$mod,${key},focusmonitor,${direction}"
          )
          directions);
    };

    # This is order sensitive, so it has to come here.
    extraConfig = ''
      env = XDG_CURRENT_DESKTOP, Hyprland
      env = XDG_SESSION_DESKTOP, Hyprland

      windowrulev2 = noinitialfocus, class:^(ghidra-Ghidra)$,title:^(win.+)$
      windowrulev2 = nofocus, class:^(ghidra-Ghidra)$,title:^ $
      windowrulev2 = windowdance, class:^(ghidra-Ghidra)$,floating:1

      windowrulev2 = noinitialfocus, class:^(sourceinsight4.exe)$,title:^(win.+)$
      windowrulev2 = nofocus, class:^(sourceinsight4.exe)$,title:^ $
      windowrulev2 = windowdance, class:^(sourceinsight4.exe)$,floating:1

      windowrulev2 = bordercolor rgb(FFFF00),pinned:1
      windowrulev2 = idleinhibit always, class:^(mpv)$


      bind = $mod, 1, workspace, 1
      bind = $mod, 2, workspace, 2
      bind = $mod, 3, workspace, 3
      bind = $mod, 4, workspace, 4
      bind = $mod, 5, workspace, 5
      bind = $mod, 6, workspace, 6
      bind = $mod, 7, workspace, 7
      bind = $mod, 8, workspace, 8
      bind = $mod, 9, workspace, 9
      bind = $mod, 0, workspace, 10
      bind = $mod, F1, workspace, 11
      bind = $mod, F2, workspace, 12
      bind = $mod, F3, workspace, 13
      bind = $mod, F4, workspace, 14
      bind = $mod, F5, workspace, 15
      bind = $mod, F6, workspace, 16
      bind = $mod, F7, workspace, 17
      bind = $mod, F8, workspace, 18
      bind = $mod, F9, workspace, 19

      bind = $mod SHIFT, 1, movetoworkspacesilent, 1
      bind = $mod SHIFT, 2, movetoworkspacesilent, 2
      bind = $mod SHIFT, 3, movetoworkspacesilent, 3
      bind = $mod SHIFT, 4, movetoworkspacesilent, 4
      bind = $mod SHIFT, 5, movetoworkspacesilent, 5
      bind = $mod SHIFT, 6, movetoworkspacesilent, 6
      bind = $mod SHIFT, 7, movetoworkspacesilent, 7
      bind = $mod SHIFT, 8, movetoworkspacesilent, 8
      bind = $mod SHIFT, 9, movetoworkspacesilent, 9
      bind = $mod SHIFT, 0, movetoworkspacesilent, 10
      bind = $mod SHIFT, F1, movetoworkspacesilent, 11
      bind = $mod SHIFT, F2, movetoworkspacesilent, 12
      bind = $mod SHIFT, F3, movetoworkspacesilent, 13
      bind = $mod SHIFT, F4, movetoworkspacesilent, 14
      bind = $mod SHIFT, F5, movetoworkspacesilent, 15
      bind = $mod SHIFT, F6, movetoworkspacesilent, 16
      bind = $mod SHIFT, F7, movetoworkspacesilent, 17
      bind = $mod SHIFT, F8, movetoworkspacesilent, 18
      bind = $mod SHIFT, F9, movetoworkspacesilent, 19
    '';
  };
}
