{pkgs, ...}: {
  programs.emacs = {
    extraPackages = epkgs:
      with epkgs; [
        vterm
        eshell-syntax-highlighting
        esh-autosuggest
      ];
    extraConfig = builtins.readFile ./shell.el;
  };
}
