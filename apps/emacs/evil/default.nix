{pkgs, ...}: {
  programs.emacs = {
    extraPackages = epkgs:
      with epkgs; [
        evil
        evil-nerd-commenter
      ];
    extraConfig = builtins.readFile ./evil.el;
  };
}
