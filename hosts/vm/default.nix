{
  config,
  lib,
  pkgs,
  inputs,
  user,
  ...
}: {
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix
    ../../nixos/general.nix
  ];

  # Use the systemd-boot EFI boot loader.
  boot.loader = {
    efi = {
      canTouchEfiVariables = true;
      efiSysMountPoint = "/boot/efi";
    };
    timeout = 2;
    systemd-boot = {
      enable = true;
      configurationLimit = 5; # Limit the amount of configurations
    };
  };

  networking.hostName = "nixos"; # Define your hostname.

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Asia/Jerusalem";

  # Select internationalisation properties.
  i18n = {
    defaultLocale = "en_US.UTF-8";
    extraLocaleSettings = {
      # Extra locale settings that need to be overwritten
      LC_TIME = "en_US.UTF-8";
      LC_MONETARY = "en_US.UTF-8";
    };
  };

  console = {
    packages = [pkgs.terminus_font];
    font = "${pkgs.terminus_font}/share/consolefonts/ter-i22b.psf.gz";
    useXkbConfig = true; # use xkbOptions in tty.
  };

  services = {
    pipewire = {
      # Sound
      enable = true;
      alsa = {
        enable = true;
        support32Bit = true;
      };
      pulse.enable = true;
    };
  };

  xdg.portal = {
    # Required for flatpak
    enable = true;
    wlr.enable = true;
    extraPortals = [pkgs.xdg-desktop-portal-gtk];
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.${user} = {
    # System User
    isNormalUser = true;
    extraGroups = ["wheel" "video" "audio" "camera" "networkmanager" "kvm" "libvirtd"];
    initialPassword = "toor";
  };

  security = {
    sudo.wheelNeedsPassword = true;
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  environment = {
    variables = {
      EDITOR = "nvim";
      VISUAL = "nvim";
      TERMINAL = "foot";
      FILEGUI = "thunar";
    };
    loginShellInit = ''
      if [ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty1" ]; then
          exec river
      fi
    '';
    interactiveShellInit = ''
      alias lg='lazygit'
    '';
    systemPackages = with pkgs; [
      # Default packages installed system-wide
      alacritty
      atool
      bat
      bemenu
      chafa
      clang
      curl
      ffmpeg
      firefox
      foot
      fzf
      git
      gvfs
      highlight
      htop
      imagemagick
      lazygit
      lf
      man
      mediainfo
      mpc-cli
      mpd
      mpv
      mtpfs
      ncmpcpp
      neovim
      newsboat
      odt2txt
      p7zip
      poppler
      pulsemixer
      river
      sc-im
      socat
      swaylock
      unrar
      unzip
      wev
      wl-clipboard
      wlr-randr
      xdg-desktop-portal
      xfce.thunar
      xwayland
      yt-dlp
    ];
  };

  fonts = {
    fonts = with pkgs; [
      cascadia-code
      noto-fonts-emoji
      font-awesome
    ];
    fontconfig = {
      enable = true;
      defaultFonts = {
        monospace = ["Cascadia Code"];
        serif = ["Noto Serif" "Source Han Serif"];
        sansSerif = ["Noto Sans" "Source Han Sans"];
      };
    };
  };

  system.stateVersion = "23.05";
}
