{
  config,
  lib,
  pkgs,
  inputs,
  ...
}: {
  imports = [
    ./hyprland.nix
    ./waybar.nix
  ];

  programs.imv = {
    enable = true;
    settings = {
      binds = {
        w = "exec ${inputs.self.packages.${pkgs.system}.setbg}/bin/setbg $imv_current_file";
      };
    };
  };

  programs.emacs = {
    package =
      if (config.graphical.option == true)
      then pkgs.emacs29-pgtk
      else pkgs.emacs-nox;
  };

  home.packages = with pkgs; [
    grim
    slurp
    swww
    wdisplays
    wf-recorder
    wl-clipboard
    wl-mirror
  ];
}
