{
  config,
  pkgs,
  inputs,
  ...
}: let
  likecritty = "${inputs.self.packages.${pkgs.system}.likecritty}/bin/likecritty";
in {
  home = {
    sessionVariables = {
      TERMINAL = "${likecritty}";
    };
  };

  home.file.".config/Thunar/uca.xml" = {
    text = ''
      <?xml version="1.0" encoding="UTF-8"?>
      <actions>
      <action>
      <icon>utilities-terminal</icon>
      <name>Open Terminal Here</name>
      <submenu></submenu>
      <unique-id>1704934536132743-1</unique-id>
      <command>${likecritty} --working-directory %f</command>
      <description>Example for a custom action</description>
      <range></range>
      <patterns>*</patterns>
      <startup-notify/>
      <directories/>
      </action>
      </actions>
    '';
    force = true;
  };

  programs.alacritty = let
    inherit (config.colorscheme) colors;
  in {
    enable = true;
    settings = {
      env.TERM = "xterm-256color";
      cursor = {
        unfocused_hollow = true;
        vi_mode_style = "Block";
        style.shape = "Beam";
      };

      window = {
        opacity = 0.9;
        padding = {
          x = 5;
          y = 5;
        };
      };
      selection.save_to_clipboard = true;
      scrolling.history = 10000;

      keyboard.bindings = [
        {
          key = "N";
          mods = "Control|Shift";
          action = "SpawnNewInstance";
        }
        {
          key = "F";
          mods = "Control|Shift";
          action = "ReceiveChar";
        }
      ];
      colors = {
        primary = {
          background = "#${colors.base00}";
          foreground = "#${colors.base05}";
        };
        normal = {
          black = "#${colors.base01}";
          white = "#${colors.base06}";
          red = "#${colors.base08}";
          yellow = "#${colors.base0A}";
          green = "#${colors.base0B}";
          cyan = "#${colors.base0C}";
          blue = "#${colors.base0D}";
          magenta = "#${colors.base0E}";
        };

        vi_mode_cursor = {
          cursor = "#${colors.base0E}";
          text = "CellBackground";
        };
      };
    };
  };
}
