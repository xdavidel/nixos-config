{
  config,
  pkgs,
  inputs,
  ...
}: {
  # register appimages
  boot.binfmt.registrations.appimage = {
    wrapInterpreterInShell = false;
    interpreter = "${pkgs.appimage-run}/bin/appimage-run";
    recognitionType = "magic";
    offset = 0;
    mask = ''\xff\xff\xff\xff\x00\x00\x00\x00\xff\xff\xff'';
    magicOrExtension = ''\x7fELF....AI\x02'';
  };

  boot.binfmt.emulatedSystems = [
    "aarch64-linux"
    "armv6l-linux"
    "armv7l-linux"
    "x86_64-windows"
  ];

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  # Configure keymap in X11
  services.xserver = {
    xkb = {
      variant = "";
      layout = "us";
    };
    libinput.enable = true;
  };

  security.polkit.enable = true;
  security.pam.services.swaylock = {};

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable gnome-keyring
  services.gnome.gnome-keyring.enable = true;

  # thumbnailer service
  services.tumbler.enable = true;

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };

  programs = {
    thunar = {
      enable = true;
      plugins = with pkgs.xfce; [
        thunar-archive-plugin
        thunar-volman
      ];
    };
  };

  programs.seahorse.enable = true;

  fonts.packages = with pkgs; [
    (nerdfonts.override {fonts = ["CascadiaCode"];})
  ];

  environment.systemPackages = with pkgs; [
    alacritty
    brave
    networkmanagerapplet
  ];

  home-manager = {
    extraSpecialArgs = {inherit inputs;};
    users.user = import ../../home/desktop;
  };
}
