{pkgs}:
pkgs.writeShellApplication {
  name = "hashes";
  runtimeInputs = with pkgs; [
    coreutils
  ];
  text = builtins.readFile ./hashes;
}
