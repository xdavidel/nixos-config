{pkgs}: let
  basename = "${pkgs.coreutils}/bin/basename";
  bemenu = "${pkgs.bemenu}/bin/bemenu";
  cut = "${pkgs.coreutils}/bin/cut";
  notify-send = "${pkgs.libnotify}/bin/notify-send";
  sed = "${pkgs.gnused}/bin/sed";
  tr = "${pkgs.coreutils}/bin/tr";
  wl-copy = "${pkgs.wl-clipboard}/bin/wl-copy";

  emoji = builtins.readFile ./emoji;
  fa = builtins.readFile ./font-awesome-list;
in
  pkgs.writeScriptBin "iconsel" ''
    #!/bin/sh

    self="$(${basename} "$0")"

    usage(){
        echo "usage: $self [options ..]

    options:
      -a     font-awesome
      -e     emoji
      -h     help
      -v     verbose"
      exit 1
    }

    case "$@" in
        *-v*) set -x ;;
    esac

    repo=""

    while getopts "aehv" o; do case "$o" in
        a) repo="${fa}" ;;
        e) repo="${emoji}" ;;
        h) usage ;;
        v)  ;;
        *) usage ;;
    esac done

    # Repo list must be specified
    [ -z "$repo" ] && echo "No repository specified" && exit 1

    chosen=$(echo "$repo" | ${cut} -d ';' -f1 | ${bemenu} -l 10 -w --fn "mono 14" | ${sed} "s/ .*//")

    [ -z "$chosen" ] && exit

    echo "$chosen" | ${tr} -d '\n' | ${wl-copy} && \
        ${notify-send} "'$chosen' copied to clipboard." &
  ''
