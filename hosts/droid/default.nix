{
  config,
  inputs,
  lib,
  pkgs,
  ...
}: let
  system = "aarch64-linux";
in {
  # Simply install just the packages
  environment.packages = with pkgs; [
    bzip2
    diffutils
    findutils
    gnugrep
    gnupg
    gnused
    gnutar
    gzip
    lunarvim
    procps
    ripgrep
    unzip
    xz
    zip
  ];

  # Backup etc files instead of failing to activate generation if a file already exists in /etc
  environment.etcBackupExtension = ".bak";

  # Read the changelog before changing this value
  system.stateVersion = "23.05";

  # Set up nix for flakes
  nix.extraOptions = ''
    experimental-features = nix-command flakes
  '';

  # Set your time zone
  time.timeZone = "Asia/Jerusalem";

  home-manager = {
    backupFileExtension = "hm-bak";
    extraSpecialArgs = {inherit inputs system;};
    useGlobalPkgs = true;

    config = ../../home/global/default.nix;
  };
}
