{
  description = "Basic nixos config using flake";

  inputs = {
    nixpkgs-stable.url = "github:nixos/nixpkgs/nixos-23.05";
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    NixOS-WSL = {
      url = "github:nix-community/NixOS-WSL";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nix-on-droid = {
      url = "github:t184256/nix-on-droid/release-23.05";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.home-manager.follows = "home-manager";
    };
    home-manager = {
      # User Package Management
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nix-colors.url = "github:misterio77/nix-colors";

    firefox-addons = {
      url = "gitlab:rycee/nur-expressions?dir=pkgs/firefox-addons";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # neovim config by nix
    nixvim = {
      url = "github:nix-community/nixvim";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    self,
    nixpkgs,
    nixvim,
    NixOS-WSL,
    nix-on-droid,
    home-manager,
    ...
  } @ inputs: let
    inherit (self) outputs;
    systems = [
      "aarch64-linux"
      "i686-linux"
      "x86_64-linux"
      "aarch64-darwin"
      "x86_64-darwin"
    ];
    forAllSystems = nixpkgs.lib.genAttrs systems;
    user = "user";
    lib = nixpkgs.lib // home-manager.lib;
  in {
    inherit lib;

    # availble using `nix build .#<package>` / via inputs
    packages = forAllSystems (system: import ./pkgs {pkgs = nixpkgs.legacyPackages.${system};});

    # availble using `nix fmt`
    formatter = forAllSystems (system: nixpkgs.legacyPackages.${system}.alejandra);

    # availble using `nix develop / nix-shell`
    devShells = forAllSystems (system:
      import ./shell.nix {
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            (final: _prev: import ./pkgs {pkgs = final;})
          ];
          config.allowUnfree = true;
        };
      });

    # availble using `nixos-rebuild --flake .#<host> switch`
    nixosConfigurations = {
      vm = lib.nixosSystem {
        specialArgs = {inherit inputs outputs;};
        modules = [
          ./hosts/vm
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.extraSpecialArgs = {inherit user;};
            home-manager.users.${user} = {
              imports = [./home/global];
            };
          }
        ];
      };
      work = lib.nixosSystem {
        modules = [./hosts/work];
        specialArgs = {inherit inputs outputs;};
      };
      t14s = lib.nixosSystem {
        modules = [./hosts/t14s];
        specialArgs = {inherit inputs outputs;};
      };
      wsl = lib.nixosSystem {
        specialArgs = {inherit inputs outputs;};
        modules = [
          ./hosts/wsl
          NixOS-WSL.nixosModules.wsl
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.extraSpecialArgs = {
              inherit user;
            };
            home-manager.users.${user} = {
              imports = [./home/global];
            };
          }
        ];
      };

      # availble using `nix build .#nixosConfigurations.rpi.config.system.build.sdImage`
      rpi = nixpkgs.lib.nixosSystem {
        system = "aarch64-linux";
        modules = [
          "${nixpkgs}/nixos/modules/installer/sd-card/sd-image-aarch64.nix"
          {
            config = {
              users.users.${user} = {
                # System User
                isNormalUser = true;
                initialPassword = "toor";
              };

              system = {
                stateVersion = "22.05";
              };
            };
          }
        ];
      };
    };

    # availble using `nix-on-droid switch --flake .`
    nixOnDroidConfigurations.default = nix-on-droid.lib.nixOnDroidConfiguration {
      extraSpecialArgs = {inherit inputs;};
      modules = [
        ./hosts/droid
      ];
    };

    # vailable using `nix build .#homeConfigurations.x86_64-linux.user.activationPackage`
    homeConfigurations = forAllSystems (system: {
      user = home-manager.lib.homeManagerConfiguration {
        pkgs = nixpkgs.legacyPackages.${system};
        extraSpecialArgs = {inherit inputs outputs;};
        modules = [./home/standalone.nix];
      };
    });
  };
}
