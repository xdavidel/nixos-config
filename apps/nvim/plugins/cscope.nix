{pkgs, ...}: {
  extraPlugins = let
    cscope_maps = pkgs.vimUtils.buildVimPlugin {
      name = "cscope_maps";
      src = pkgs.fetchFromGitHub {
        owner = "dhananjaylatkar";
        repo = "cscope_maps.nvim";
        rev = "065fcdd059eb69d013a10d94846d20f1b8fd8ef6";
        sha256 = "xl9eDmBCOxJ0lNHDdnHxonHMn1Iz5lW/oUOdwoRknH4=";
      };
    };
  in
    with pkgs.vimPlugins; [
      cscope_maps
    ];
  extraConfigLua = ''
    require('cscope_maps').setup {
      disable_maps = true,
      cscope = {
        picker = 'telescope',
        exec = 'cscope',
      },
    }

    local dir = vim.fn.getcwd()
    vim.keymap.set('n', '<leader>cb', function()
      vim.fn.jobstart(
        '${pkgs.fd}/bin/fd -e c -e h -e cpp -e hpp > cscope.files',
        {
          cwd = dir,
          on_exit = function(jobid, data, event)
            vim.fn.jobstart('cscope -b', {
              cwd = dir,
              on_exit = function(jobid, data, event) vim.print('cscope build successfully.') end,
              on_stdout = function(jobid, data, event) end,
              on_stderr = function(jobid, data, event) end
            }) end,
          on_stdout = function(jobid, data, event) end,
          on_stderr = function(jobid, data, event) end
        })
    end, { desc = '[C]scope [B]uild' })

    local group = vim.api.nvim_create_augroup('Cscope', { clear = true })
    vim.api.nvim_create_autocmd('BufReadPost', {
      pattern = { '*.c', '*.h', '*.cpp', '*.hpp' },
      callback = function(event)
        vim.keymap.set('n', '<leader>cr', function()
          require('cscope_maps').cscope_prompt('c', vim.fn.expand '<cword>')
        end, { buffer = event.buf, desc = '[C]scope [R]eferences' })
        vim.keymap.set('n', '<leader>cd', function()
          require('cscope_maps').cscope_prompt('g', vim.fn.expand '<cword>')
        end, { buffer = event.buf, desc = '[C]scope [D]efinition' })
        vim.keymap.set('n', '<leader>cs', function()
          require('cscope_maps').cscope_prompt('s', vim.fn.expand '<cword>')
        end, { buffer = event.buf, desc = '[C]scope [S]ymbol' })
        vim.keymap.set('n', '<leader>cc', function()
          require('cscope_maps').cscope_prompt('d', vim.fn.expand '<cword>')
        end, { buffer = event.buf, desc = '[C]scope [C]alls' })
        vim.keymap.set('n', '<leader>cv', function()
          require('cscope_maps').cscope_prompt('s', vim.fn.expand '<cword>')
        end, { buffer = event.buf, desc = '[C]scope [V]alue' })
      end,
      group = group,
    })
  '';

  extraPackages = with pkgs; [cscope];
}
