{pkgs}:
pkgs.writeShellApplication {
  name = "togglefloatfocus";
  runtimeInputs = with pkgs; [
    jq
  ];
  text = builtins.readFile ./togglefloatfocus;
}
