{
  config.plugins = {
    treesitter = {
      enable = true;
      ensureInstalled = [
        "bash"
        "c"
        "c_sharp"
        "cmake"
        "cpp"
        "css"
        "dart"
        "dockerfile"
        "git_config"
        "gitcommit"
        "gitignroe"
        "go"
        "haskell"
        "html"
        "ini"
        "java"
        "javascript"
        "jsdoc"
        "json"
        "json5"
        "jsonc"
        "jsonnet"
        "kotlin"
        "llvm"
        "lua"
        "luadoc"
        "make"
        "markdown"
        "markdown_inline"
        "meson"
        "ninja"
        "nix"
        "ocaml"
        "org"
        "passwd"
        "proto"
        "python"
        "regex"
        "rust"
        "scss"
        "sql"
        "toml"
        "typescript"
        "vim"
        "yaml"
        "zig"
      ];
      indent = true;
      folding = true;
    };

    treesitter-textobjects = {
      enable = true;
      select = {
        enable = true;
        keymaps = {
          "af" = "@function.outer";
          "if" = "@function.inner";
          "ac" = "@class.outer";
          "ic" = "@class.inner";
        };
        includeSurroundingWhitespace = true;
        lookahead = true;
        selectionModes = {
          "@parameter.outer" = "v"; # charwise
          "@function.outer" = "V"; # linewise
          "@class.outer" = "<c-v>"; # blockwise
        };
      };
    };

    treesitter-context = {enable = false;};
  };
}
