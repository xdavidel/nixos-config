{pkgs}:
pkgs.writeShellApplication {
  name = "flakeup";
  runtimeInputs = with pkgs; [
    jq
    gnused
    fzf
  ];
  text = builtins.readFile ./flakeup;
}
