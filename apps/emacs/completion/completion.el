(use-package vertico
  :demand
  :config
  (vertico-mode)
  (setq vertico-cycle t)
  :custom-face
  (vertico-current ((t (:background "#3a3f5a")))))

(use-package marginalia
  :config
  (marginalia-mode 1)
  :custom
  (marginalia-annotators '(marginalia-annotators-heavy marginalia-annotators-light nil)))

;; Set up Orderless for better fuzzy matching
(use-package orderless
  :init
  (setq completion-category-defaults nil
        read-file-name-completion-ignore-case t)
  :custom
  (completion-styles '(orderless))
  (completion-category-overrides '((file (styles . (partial-completion)))
                                   (minibuffer (initials)))))

(use-package consult
  :demand
  :after evil
  :bind (
   :map global-map
   ("C-c o" . consult-imenu)     ; navigation by "imenu" items
   ("C-c r" . consult-ripgrep)   ; search file contents
   ("C-S-f" . consult-ripgrep)   ; search file contents
   ("C-x b" . consult-buffer)    ; enhanced switch to buffer
   ("M-o"   . consult-outline)   ; navigation by headings
   ("C-s"   . consult-line)      ; search lines with preview
   ("M-y"   . consult-yank-pop) ; editing cycle through kill-ring
   ("C-p"   . consult-fd) ; editing cycle through kill-ring
   :map minibuffer-local-completion-map
   ("<tab>"  . minibuffer-force-complete)
   :map evil-normal-state-map
   ("<leader> fb"  . consult-buffer)
   ("<leader> fr"  . consult-recent-file)
   ("<leader> sc"  . consult-theme)
   ("<leader> sR"  . consult-register)
   ("<leader> st"  . consult-ripgrep))
  :hook (completion-setup . hl-line-mode)
  :custom
  (consult-preview-raw-size 1) ;; Simple preview - no hooks
  (completion-in-region-function #'consult-completion-in-region)
  :config
  ;; configure preview behavior
  (consult-customize
   consult-buffer consult-bookmark :preview-key '(:debounce 0.5 any)
   consult-theme :preview-key '(:debounce 1 any)
   consult-line :preview-key '(:debounce 0 any))

  ;; Use Consult to select xref locations with preview
  (require 'consult-xref)
  (setq xref-show-xrefs-function #'consult-xref
        xref-show-definitions-function #'consult-xref)

  (setq consult-project-root-function
        (lambda ()
          (when-let (project (project-current))
            (car (project-roots project))))))

;; Completion framwork for anything
(use-package company
  :hook
  (prog-mode . company-mode)
  (org-mode  . company-mode)
  (text-mode . company-mode)
  :custom
  (company-idle-delay 1)
  (company-minimum-prefix-length 1)
  (company-tooltip-align-annotations t)
  (company-dabbrev-downcase nil)
  (company-selection-wrap-around t)
  (company-format-margin-function #'company-vscode-dark-icons-margin))

;; Better sotring company results
(use-package company-prescient
  :hook
  (company-mode . company-prescient-mode))

(use-package company-emoji
  :hook
  (company-mode . (lambda() (company-emoji-init))))
