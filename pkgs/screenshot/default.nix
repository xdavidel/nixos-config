{pkgs}:
pkgs.writeShellApplication {
  name = "screenshot";
  runtimeInputs = with pkgs; [
    bemenu
    grim
    imagemagick
    jq
    swappy
    wl-clipboard
  ];
  text = builtins.readFile ./screenshot;
}
