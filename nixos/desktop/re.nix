{
  config,
  pkgs,
  inputs,
  ...
}: {
  # Add user to libvirtd group
  users.users.user.extraGroups = ["wireshark"];

  programs.wireshark.enable = true;

  environment.systemPackages = with pkgs; [
    # (cutter.withPlugins (ps: with ps; [jsdec rz-ghidra sigdb]))
    (rizin.withPlugins (ps: with ps; [rz-ghidra]))
    ghidra
    imhex
    wireshark
    zap
    inputs.self.packages.${pkgs.system}.binrev
  ];
}
