{pkgs, ...}: {
  imports = [
    ./plugins/cscope.nix
    ./plugins/telescope.nix
    ./plugins/treesitter.nix
    ./plugins/neogit.nix
    ./plugins/arachne.nix
    ./plugins/plantuml.nix
  ];

  extraPlugins = with pkgs.vimPlugins; [
    bigfile-nvim
    codi-vim # Code Interaction
    tabular
    vim-cool # clear search highlight after move
  ];

  plugins = {
    cursorline = {
      enable = true;
      cursorline = {
        enable = true;
        timeout = 0;
        number = true;
      };
      cursorword = {
        enable = true;
        hl = {link = "LspReferenceText";};
      };
    };

    nix.enable = true;

    lualine = {
      enable = true;
      iconsEnabled = true;
      extensions = ["nvim-tree"];
    };

    todo-comments = {
      enable = true;
      signs = false;
    };

    noice = {
      enable = true;
      presets = {
        bottom_search = true;
        command_palette = false;
        lsp_doc_border = true;
        long_message_to_split = true;
      };
      cmdline.view = "cmdline";
    };

    nvim-colorizer.enable = true;

    nvim-autopairs.enable = true;
    nvim-tree.enable = true;

    gitsigns.enable = true;

    comment.enable = true;

    surround.enable = true;

    toggleterm = {
      enable = true;
      size = ''
        function (term)
          if term.direction == "horizontal" then
            return vim.o.lines * 0.8
          elseif term.direction == "vertical" then
            return vim.o.columns * 0.4
          end
        end
      '';
      openMapping = "<C-\\>";
    };
    zig = {
      enable = true;
      settings.fmt_autosave = false;
    };
  };
}
