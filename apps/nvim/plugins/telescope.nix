{pkgs, ...}: {
  plugins.telescope = {
    enable = true;
    extensions = {
      fzf-native.enable = true; # so fzf gets in teh path
      file_browser.enable = true;
    };
  };
  extraConfigLua = ''
    local is_git_repo = function()
      vim.fn.system("git rev-parse --is-inside-work-tree")

      return vim.v.shell_error == 0
    end

    local get_git_root = function ()
      local dot_git_path = vim.fn.finddir(".git", ".;")
      return vim.fn.fnamemodify(dot_git_path, ":h")
    end

    function register(key_map)
      key_map.options = key_map.options and key_map.options or {}
      key_map.options.desc = key_map.description
      vim.keymap.set(key_map.mode, key_map.lhs, key_map.rhs, key_map.options)
    end

    function bulk_register(group_keymap, opts)
      for _, key_map in pairs(group_keymap) do
        if opts then
          key_map.options = opts
        end
        register(key_map)
      end
    end

    local telescope = require('telescope')

    telescope.setup {
      defaults = {
        file_ignore_patterns = {
          ".git/",
          "cscope.out",
          ".ccls",
          "tags",
          "TAGS",
          "node_modules",
        },
      },
      mappings = {
        i = {
          ['<C-u>'] = false,
          ['<C-d>'] = false,
        },
      },
    }

    bulk_register({
      {
        mode = {'n', 'i'},
        lhs = '<C-p>',
        rhs = function ()
          local opts = {}
          if is_git_repo() then
            opts.cwd = get_git_root()
          end
          require"telescope.builtin".find_files(opts)
        end,
        options = { noremap = true, silent = true },
        description = "Find Files",
      },
      {
        mode = {'n', 'i'},
        lhs = '<A-x>',
        rhs = [[<cmd>Telescope commands<cr>]],
        options = { noremap = true, silent = true },
        description = "Commands",
      },
      {
        mode = {'n', 'i'},
        lhs = '<C-f>',
        rhs = [[<cmd>Telescope current_buffer_fuzzy_find<cr>]],
        options = { noremap = true, silent = true },
        description = "Find",
      },

      {
        mode = {'n'},
        lhs = '<leader>ff',
        rhs = function()
          local opts = {
            cwd = vim.fn.expand("$HOME"),
            prompt_title = "Explorer",
            hidden = true,
          }
          telescope.extensions.file_browser.file_browser(opts)
        end,
        options = { noremap = true, silent = true },
        description = "[F]ile [F]ind",
      },
      {
        mode = {'n'},
        lhs = '<leader>fd',
        rhs = function()
          local opts = {
            prompt_title = "Files",
            hidden = true,
            depth = 2,
          }
          telescope.extensions.file_browser.file_browser(opts)
        end,
        options = { noremap = true, silent = true },
        description = "[F]ile [D]irectory",
      },
      {
        mode = {'n'},
        lhs = '<leader>sl',
        rhs = require('telescope.builtin').resume,
        options = { noremap = true, silent = true },
        description = "[S]earch [L]ast",
      },
      {
        mode = {'n'},
        lhs = '<leader>bs',
        rhs = require('telescope.builtin').buffers,
        options = { noremap = true, silent = true },
        description = "[B]uffer [S]witch",
      },
      {
        mode = {'n'},
        lhs = '<leader>fr',
        rhs = require('telescope.builtin').oldfiles,
        options = { noremap = true, silent = true },
        description = "[F]ile [R]ecent",
      },
      {
        mode = {'n'},
        lhs = '<leader>fn',
        rhs = function ()
          require('telescope.builtin').find_files({
            cwd = vim.fn.stdpath("config"),
            hidden = true,
          })
        end,
        options = { noremap = true, silent = true },
        description = "[F]ile [N]eovim files",
      },
      {
        mode = {'n'},
        lhs = '<leader>gb',
        rhs = require('telescope.builtin').git_branches,
        options = { noremap = true, silent = true },
        description = "[G]it [B]ranches",
      },
      {
        mode = {'n'},
        lhs = '<leader>sc',
        rhs = require('telescope.builtin').colorscheme,
        options = { noremap = true, silent = true },
        description = "[S]earch [C]olorscheme",
      },
      {
        mode = {'n'},
        lhs = '<leader>sC',
        rhs = function()
          require('telescope.builtin').colorscheme({enable_preview = true})
        end,
        options = { noremap = true, silent = true },
        description = "[S]earch [C]olorscheme (preview)",
      },
      {
        mode = {'n'},
        lhs = '<leader>sh',
        rhs = require('telescope.builtin').help_tags,
        options = { noremap = true, silent = true },
        description = "[S]earch [H]elp",
      },
      {
        mode = {'n'},
        lhs = '<leader>sm',
        rhs = require('telescope.builtin').man_pages,
        options = { noremap = true, silent = true },
        description = "[S]earch [M]anpage",
      },
      {
        mode = {'n'},
        lhs = '<leader>sr',
        rhs = require('telescope.builtin').registers,
        options = { noremap = true, silent = true },
        description = "[S]earch [R]egisters",
      },
      {
        mode = {'n'},
        lhs = '<leader>st',
        rhs = function ()
          local opts = {}
          if is_git_repo() then
            opts = {
              cwd = get_git_root(),
            }
          end
          require("telescope.builtin").live_grep(opts)
        end,
        options = { noremap = true, silent = true },
        description = "[S]earch [T]ext",
      },
      {
        mode = {'n'},
        lhs = '<leader>ss',
        rhs = function ()
          local opts = {}
          if is_git_repo() then
            opts = {
              cwd = get_git_root(),
            }
          end
          require('telescope.builtin').grep_string(opts)
        end,
        options = { noremap = true, silent = true },
        description = "[S]earch [S]elected",
      },
    })
  '';
}
