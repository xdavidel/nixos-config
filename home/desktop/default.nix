{
  config,
  osConfig,
  lib,
  pkgs,
  ...
}: {
  imports = [
    ../global
    ../desktop/common/alacritty.nix
    ../desktop/common/firefox.nix
    ../desktop/common/gtk-qt.nix
    ../desktop/wayland
  ];

  graphical.option = true;

  xdg = {
    configFile."mimeapps.list".force = true;
    userDirs = {
      enable = true;
      createDirectories = true;
    };
    mimeApps = {
      enable = true;
      defaultApplications = let
        brave = "brave-browser.desktop";
        zathura = "org.pwmt.zathura.desktop";
        imv = "imv-dir.desktop";
        nvim = "nvim-desktop.desktop";
      in {
        "text/html" = "${brave}";
        "x-scheme-handler/http" = "${brave}";
        "x-scheme-handler/https" = "${brave}";
        "x-scheme-handler/about" = "${brave}";
        "x-scheme-handler/unknown" = "${brave}";

        "application/postscript" = "${zathura}";
        "application/pdf" = "${zathura}";

        "image/svg+xml" = "${imv}";
        "image/png" = "${imv}";
        "image/jpeg" = "${imv}";
        "image/webp" = "${imv}";
        "image/gif" = "${imv}";

        "text/plain" = "${nvim}";
        "text/x-shellscript" = "${nvim}";
      };
    };
  };


  dconf.settings = {
    "org/gnome/desktop/interface" = {
      color-scheme = "prefer-dark";
    };
  };

  services.dunst = let
    inherit (config.colorscheme) colors;
    msgbox = "${pkgs.papirus-icon-theme}/share/icons/Papirus/48x48/status/messagebox_";
  in {
    enable = true;
    settings = {
      global = {
        follow = "mouse";
        notification_height = 0;
        separator_height = 1;
        separator_color = "auto";
        sort = "yes";
        padding = 8;
        frame_width = 3;
        width = 300;
        height = 300;
        offset = "30x50";
        origin = "top-right";
        transparency = 15;
        frame_color = "#eceff1";
        font = "Mono 12";
        line_height = 0;
        markup = "full";
        format = "<b>%s</b>\n%b";
        alignment = "left";
        word_wrap = "yes";
        ellipsize = "middle";
        ignore_newline = "no";
        corner_radius = 5;
        mouse_left_click = "do_action";
        mouse_middle_click = "close_all";
        mouse_right_click = "close_current";
      };
      urgency_low = {
        background = "#${colors.base04}";
        foreground = "#${colors.base05}";
        frame_color = "#${colors.base00}";
        timeout = 5;
        icon = "${msgbox}info.svg";
      };

      urgency_normal = {
        background = "#${colors.base01}";
        foreground = "#${colors.base05}";
        frame_color = "#${colors.base00}";
        timeout = 5;
        icon = "${msgbox}info.svg";
      };

      urgency_critical = {
        background = "#${colors.base08}";
        foreground = "#${colors.base02}";
        frame_color = "#${colors.base00}";
        icon = "${msgbox}critical.svg";
      };
    };
  };

  services.playerctld.enable = true;

  services.mpd = {
    enable = true;
    # network.port = 6600;
    network.startWhenNeeded = true;
    musicDirectory = "~/Music";
    extraConfig = ''
      audio_output {
        type "pipewire"
        name "Pipewire Playback"
      }
    '';
  };

  services.mpd-mpris.enable = true;

  services.network-manager-applet.enable = true;

  services.blueman-applet.enable = osConfig.services.blueman.enable;

  services.syncthing.enable = true;

  programs.ncmpcpp = {
    enable = true;
    bindings = [
      {
        key = "j";
        command = "scroll_down";
      }
      {
        key = "k";
        command = "scroll_up";
      }
      {
        key = "m";
        command = "move_sort_order_down";
      }
      {
        key = "M";
        command = "move_sort_order_up";
      }
      {
        key = "+";
        command = "show_clock";
      }
      {
        key = "=";
        command = "volume_up";
      }
      {
        key = "left";
        command = "previous_column";
      }
      {
        key = "h";
        command = "previous_column";
      }
      {
        key = "left";
        command = "jump_to_parent_directory";
      }
      {
        key = "h";
        command = "jump_to_parent_directory";
      }
      {
        key = "right";
        command = "next_column";
      }
      {
        key = "l";
        command = "next_column";
      }
      {
        key = "right";
        command = "enter_directory";
      }
      {
        key = "l";
        command = "enter_directory";
      }
      {
        key = "n";
        command = "next_found_item";
      }
      {
        key = "N";
        command = "previous_found_item";
      }
      {
        key = "g";
        command = "move_home";
      }
      {
        key = "G";
        command = "move_end";
      }
    ];
    settings = {
      allow_for_physical_item_deletion = "yes";
    };
  };

  programs.mpv = {
    enable = true;
    bindings = {
      UP = "               osd-msg     add     volume      +2";
      DOWN = "             osd-msg     add     volume      -2";
      j = "                osd-msg     add     volume      -2";
      k = "                osd-msg     add     volume      +2";
      LEFT = "             osd-msg     seek                -5";
      RIGHT = "            osd-msg     seek                +5";
      h = "                osd-msg     seek                -5";
      l = "                osd-msg     seek                +5";
      HOME = "             osd-msg     seek                 0 absolute-percent";
      TAB = "              osd-msg     revert_seek";

      WHEEL_UP = "         osd-msg     add     volume      +2";
      WHEEL_DOWN = "       osd-msg     add     volume      -2";
      "Ctrl+WHEEL_UP" = "  osd-msg     seek                +5";
      "Ctrl+WHEEL_DOWN" = "osd-msg     seek                -5";

      L = "                osd-msg     ab-loop";

      n = "                osd-msg     playlist-next";
      p = "                osd-msg     playlist-prev";

      ">" = "              add         audio-delay         0.100";
      "<" = "              add         audio-delay        -0.100";

      "]" = "              add         video-zoom          0.1";
      "[" = "              add         video-zoom         -0.1";
      "\\" = "             set         video-zoom          0";
      "}" = "              add         speed               0.1";
      "{" = "              add         speed              -0.1";

      f = "                cycle       fullscreen";
      ENTER = "            cycle       fullscreen";
      MBTN_LEFT_DBL = "    cycle       fullscreen";
      MBTN_RIGHT = "       cycle       pause";
      MOUSE_BTN1 = "       quit";
      SPACE = "            cycle       pause";

      s = "                async       screenshot";
      S = "                async       screenshot          video";

      q = "                quit";
      Q = "                quit_watch_later";
      t = "                show_text $${track-list}";
    };
    config = {
      save-position-on-quit = true;
      osc = "no";
    };
    scripts = with pkgs.mpvScripts; [
      autoload
      mpris
      thumbnail
    ];
  };

  programs.zathura = {
    enable = true;
    mappings = {
      u = "scroll half-up";
      d = "scroll half-down";
      D = "toggle_page_mode";
      r = "reload";
      R = "rotate";
      K = "zoom in";
      J = "zoom out";
      i = "recolor";
      p = "print";
    };
    options = {
      sandbox = "none";
      statusbar-h-padding = 0;
      statusbar-v-padding = 0;
      page-padding = 1;
      selection-clipboard = "clipboard";
    };
  };

  home.packages = with pkgs; [
    telegram-desktop
  ];
}
