;;; init.el --- Emacs main configuration file -*- lexical-binding: t -*-

(message "Hello from init")

;; Change emacs user directory to keep the real one clean
(defvar emacs-cache-dir (let* ((cache-dir-xdg (getenv "XDG_CACHE_HOME"))
                                  (cache-dir (if cache-dir-xdg (concat cache-dir-xdg "/emacs")
                                               (expand-file-name "~/.cache/emacs"))))
                             (mkdir cache-dir t)
                             cache-dir)
  "A directory to save Emacs cache files.")

(defun buffer-binary-p (&optional buffer)
  "Return whether BUFFER or the current buffer is binary.
A binary buffer is defined as containing at least on null byte.
Returns either nil, or the position of the first null byte."
  (with-current-buffer (or buffer (current-buffer))
    (save-excursion
      (goto-char (point-min))
      (search-forward (string ?\x00) nil t 1))))

(defun toggle-line-wrap ()
  "Switch between line wraps."
  (interactive)
  (setq truncate-lines (not truncate-lines)))

(defun sudo-save ()
  "Save this file as super user."
  (interactive)
  (if (not buffer-file-name)
      (write-file (concat "/sudo:root@localhost:" (read-file-name "File:")))
    (write-file (concat "/sudo:root@localhost:" buffer-file-name))))

;; Minimizes GC interferecen with user activity
(use-package gcmh
  :demand
  :config
  (setq gcmh-idle-delay 5
        gcmh-high-cons-threshold (* 16 1024 1024)) ; 16M
  (gcmh-mode 1))

(use-package emacs
  :bind (:map global-map
  ;; Increase / Decrease font
  ("C-=" . text-scale-increase)
  ("C--" . text-scale-decrease)
  ("<C-mouse-4>" . text-scale-increase)
  ("<C-mouse-5>" . text-scale-decrease))
  :config
  ;; Revert Dired and other buffers
  (setq global-auto-revert-non-file-buffers t)
  ;; Revert buffers when the underlying file has changed
  (global-auto-revert-mode 1))

;; Display keys in a menu
(use-package which-key
  :diminish which-key-mode
  :demand
  :init
  (setq which-key-idle-delay 0.5)
  :config
  ;; Shows available keybindings after you start typing.
  (which-key-mode 1))

;; No line wrap be default
(setq-default truncate-lines t)

;; Use spaces instead of tabs
(setq-default indent-tabs-mode nil)

;; Tabs is this much spaces
(setq-default tab-width 4)

;; No Lock Files
(customize-set-variable 'create-lockfiles nil)

;; slow down update ui
(customize-set-variable 'idle-update-delay 1.0)

;; Don't blink the cursor
(blink-cursor-mode -1)

;; Increase autosave times
(setq auto-save-interval 2400
      auto-save-timeout 300)

;; create directory for auto-save-mode
(let* ((auto-save-path (concat emacs-cache-dir "/auto-saves/"))
       (auto-save-sessions-path (concat auto-save-path "sessions/")))
  (make-directory auto-save-path t)
  (setq auto-save-list-file-prefix auto-save-sessions-path
        auto-save-file-name-transforms `((".*" ,auto-save-path t))))

;; Put backups elsewhere
(setq backup-directory-alist
      `(("." . ,(concat emacs-cache-dir "backup")))
      backup-by-copying t     ; Use copies
      version-control t       ; Use version numbers on backups
      delete-old-versions t   ; Automatically delete excess backups
      kept-new-versions 10    ; Newest versions to keep
      kept-old-versions 5     ; Old versions to keep
      backup-enable-predicate
      (lambda (name)
        (and (normal-backup-enable-predicate name)
             (not
              (let ((method (file-remote-p name 'method)))
                (when (stringp method)
                  (member method '("su" "sudo" "doas"))))))))


;; N.B. Emacs 28 has a variable for using short answers, which should
;; be preferred if using that version or higher.
(if (boundp 'use-short-answers)
    (setq use-short-answers t)
  (fset 'yes-or-no-p 'y-or-n-p))

;; Make UTF-8 the default coding system
(set-language-environment "UTF-8")
(setq selection-coding-system 'utf-8)

;; Don't render cursors or regions in non-focused windows.
(setq-default cursor-in-non-selected-windows nil)
(setq highlight-nonselected-windows nil)

;; Rapid scrolling over unfontified regions.
(setq fast-but-imprecise-scrolling t)

;; Font compacting can be terribly expensive.
(setq inhibit-compacting-font-caches t
      redisplay-skip-fontification-on-input t)

;; Recent files
(use-package recentf
  :hook (after-init . recentf-mode)
  :init
  (setq recentf-max-saved-items 200
        recentf-auto-cleanup 'never) ;; disable before we start recentf!
  :config
  (recentf-mode 1)
  :custom
  (recentf-save-file (expand-file-name "recentf" emacs-cache-dir))
  (recentf-keep '(file-remote-p file-readable-p))
  (recentf-exclude '((expand-file-name package-user-dir)
                     ".mp4"
                     "/ssh.*"
                     ".cache"
                     ".cask"
                     ".elfeed"
                     "bookmarks"
                     "cache"
                     "ido.*"
                     "persp-confs"
                     "recentf"
                     "undo-tree-hist"
                     "url"
                     "COMMIT_EDITMSG\\'")))

;; Do not saves duplicates in kill-ring
(setq kill-do-not-save-duplicates t)

;; Make scrolling less stuttered
(setq auto-window-vscroll nil)
(setq fast-but-imprecise-scrolling t)
(setq scroll-conservatively 101)
(setq scroll-margin 5)
(setq scroll-preserve-screen-position t)

;; Middle-click paste at point, not at cursor.
(setq mouse-yank-at-point t)

;; Better support for files with long lines
;; Handle long lines in files
(use-package so-long
  :init
  (setq so-long-threshold 400) ; reduce false positives w/ larger threshold
  (global-so-long-mode 1)
  :config
  (add-to-list 'so-long-variable-overrides '(font-lock-maximum-decoration . 1))
  (add-to-list 'so-long-variable-overrides '(save-place-alist . nil)))

;; Handle large files
(use-package vlf)

;; Make shebang (#!) file executable when saved
(add-hook 'after-save-hook #'executable-make-buffer-file-executable-if-script-p)

;; Enable savehist-mode for an command history
(use-package savehist
  :init (savehist-mode 1)
  :custom
  (savehist-file (expand-file-name "history" emacs-cache-dir)))

;; Open files in last editing place
(use-package saveplace
  :init
  (save-place-mode 1)
  :config
  (setq save-place-forget-unreadable-files nil))

;; whitespace
(use-package whitespace
  :custom
  (whitespace-style '(face tabs empty trailing tab-mark indentation::space))
  (whitespace-action '(cleanup auto-cleanup))
  :hook
  (prog-mode . whitespace-mode)
  (text-mode . whitespace-mode))

;; Move lines around easily
(use-package drag-stuff)

;; Format collection for every mode
(use-package format-all)

;; Adjusting the calendar
(use-package calendar
  :config
  ;; Remove unneeded holidays
  (setq holiday-oriental-holidays nil
        holiday-bahai-holidays nil
        holiday-islamic-holidays nil
        holiday-solar-holidays nil
        holiday-christian-holidays nil)
  (setq calendar-week-start-day 0))


(provide 'init)
;;; init.el ends here
