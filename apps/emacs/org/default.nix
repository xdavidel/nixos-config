{pkgs, ...}: {
  programs.emacs = {
    extraPackages = epkgs:
      with epkgs; [
        org-appear
        toc-org
      ];
    extraConfig = builtins.readFile ./org.el;
  };
}
